import { useMemo } from "react";

export default function debounce<T extends Function>(func: T): T {
  let id: NodeJS.Timeout | undefined = undefined;
  // @ts-ignore
  let that = this as any;
  return function (...args: any[]) {
    if (id) clearTimeout(id);
    id = setTimeout(() => {
      func.apply(that, args);
    }, 500);
  } as unknown as T;
}
