import { NextPage } from "next";
import RequireAuthenticated from "../components/RequireAuthenticated";
import { ReactElement, ReactNode } from "react";
import { PageWithLayout } from "../pages/_app";

export default function requireAuth<T>(
  page: PageWithLayout<T>
): PageWithLayout<T> {
  const old = page.getLayout;
  page.getLayout = (page) => (
    <RequireAuthenticated>
      {(old ?? ((page) => page))(page)}
    </RequireAuthenticated>
  );
  return page;
}
