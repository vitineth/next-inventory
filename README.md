This is a basic digital asset management project as an attempt to get familiar with Next.JS as a framework for web development. This was built over a few days worth of work and as such is not perfect and shows a sense of gradual refinement. I wanted to limit myself on the amount of time I could put into this until it was useable. A small demo video is shown below and a feature list. There is a lot of refinement that is needed to this but it exists as an initial solution to the problem and hopefully shows some familiarity with Next.js

## Demo

![demo](.assets/demo.webm)

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Features

* Authentication via Keycloak (via NextAuth)
* Semi-structured assets, tags, and libraries
* Fully functional API
* Search with tag and library filtering
* Photo and link support
* Data pipelines
* Basic localisation support
