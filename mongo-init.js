db = db.getSiblingDB("next-inventory");
console.log("Got database");

db.createUser({
  user: "next-inventory",
  pwd: "next-inventory-pw",
  roles: [{ role: "readWrite", db: "next-inventory" }],
  mechanisms: ["SCRAM-SHA-1"],
});
console.log("Established user");

db.createCollection("tags");
console.log("Created collections");

db.assets.createIndex({
  "$**": "text",
});
console.log("Created index");
