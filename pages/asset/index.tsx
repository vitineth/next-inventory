import { GetServerSideProps, NextPage } from "next";
import SharedHeader from "../../components/SharedHeader";

import "semantic-ui-css/semantic.min.css";
import {
  Button,
  ButtonGroup,
  Container,
  Icon,
  Input,
  Label,
  Table,
} from "semantic-ui-react";
import Link from "next/link";
import FormattedTable, { TableSchema } from "../../components/FormattedTable";
import { getAssetSchema, getTagsSchema } from "../../lib/dao/settings";
import { Asset } from "../../@types/Asset";
import { getAllAssets } from "../../lib/dao/assets";
import { ShallowTag } from "../../@types/Tag";
import { useEffect, useMemo, useRef, useState } from "react";
import { CountAPIData } from "../api/count";
import PromiseLoader from "../../components/Loader";
import { ShallowLibrary } from "../../@types/Library";
import Fuse from "fuse.js";
import {
  SupplementalAssetPostSchema,
  SupplementalAssetPreSchema,
} from "../../components/SupplementalSchemas";
import bindWithDefaultLayout from "../../components/DefaultLayout";
import requireAuth from "../../utils/authentication";
import { FormattedMessage, useIntl } from "react-intl";
import debounce from "../../utils/debounce";

type AssetPropType = {
  schema: TableSchema;
  assets: Asset[];
};

export const getServerSideProps: GetServerSideProps<AssetPropType> = async (
  context
) => {
  const [schema, assets] = await Promise.all([
    getAssetSchema(),
    getAllAssets(null),
  ]);
  return {
    props: {
      schema,
      assets,
    },
  };
};

const Index: NextPage<AssetPropType> = ({ schema, assets }) => {
  const intl = useIntl();
  const [data, setData] = useState<null | Asset[]>(null);
  const [page, setPage] = useState<null | string>(null);
  const [pageStack, setPageStack] = useState<(string | null)[]>([]);
  const [search, setSearch] = useState<
    { query?: string; tags?: string[]; libraries?: string[] } | undefined
  >(undefined);
  const searchRef = useRef<Input>(null);
  const processor = useMemo(() => {
    return data === null
      ? { search: () => [] }
      : new Fuse(data, {
          keys: [
            "identifier",
            ...schema.columns
              .filter((e) => e.format === "text" || e.format === "code")
              .map((e) => e.access),
          ],
        });
  }, [data, schema]);

  useEffect(() => {
    const params = new URLSearchParams();
    params.set("compressed", "true");
    params.set("page", String(page));
    if (search) {
      if (search.query) params.set("search", encodeURIComponent(search.query));
      if (search.tags)
        params.set("tags", search.tags.map(encodeURIComponent).join(","));
      if (search.libraries)
        params.set(
          "libraries",
          search.libraries.map(encodeURIComponent).join(",")
        );
    }
    fetch(`api/asset?${params.toString()}`)
      .then((res) => res.json())
      .then((data) => {
        setData(data.data);
      });
  }, [page, search]);

  const clone = [
    ...SupplementalAssetPreSchema,
    ...schema.columns,
    ...SupplementalAssetPostSchema,
  ];

  const func = debounce((val: string) => {
    const filters: ({ tag: string } | { library: string })[] = [];
    if (val !== "") {
      // Process out any filters
      let tokens: string[] = [];
      let inQuotes = false;
      let active = "";
      for (let i = 0; i < val.length; i++) {
        if (val.charAt(i) === " " && !inQuotes) {
          tokens.push(active);
          active = "";
        } else if (val.charAt(i) === '"' && inQuotes) {
          inQuotes = false;
        } else if (val.charAt(i) === '"' && !inQuotes) {
          inQuotes = true;
        } else {
          active += val.charAt(i);
        }
      }
      if (active !== "") tokens.push(active);

      let tags: string[] = [];
      let libraries: string[] = [];

      tokens = tokens.filter((e) => {
        if (e.startsWith("tag:")) {
          tags.push(e.slice(4));
          return false;
        } else if (e.startsWith("library:")) {
          libraries.push(e.slice(8));
          return false;
        }

        return true;
      });

      const query = tokens.join(" ").trim();
      if (query.length > 0 || tags.length > 0 || libraries.length > 0) {
        setSearch({
          query: query.length === 0 ? undefined : query,
          tags: tags.length === 0 ? undefined : tags,
          libraries: libraries.length === 0 ? undefined : libraries,
        });
      } else {
        setSearch(undefined);
      }

      // Clear page after search to get fresh results
      setPage(null);
    }
  });

  return (
    <>
      <Input
        icon={<Icon name="search" inverted circular link />}
        placeholder={intl.formatMessage({ defaultMessage: "Fuzzy Search..." })}
        fluid
        // value={search}
        ref={searchRef}
        onChange={(_, data) => {
          //setSearch(data.value);
          func(data.value);
        }}
      />
      <div
        style={{
          textAlign: "right",
          color: "gray",
          fontStyle: "italic",
          marginTop: "4px",
        }}
      >
        <FormattedMessage defaultMessage={"Need help? Check out"} />{" "}
        <Link href={"/help/search"}>
          <a>
            <FormattedMessage defaultMessage={"the search help page"} />
          </a>
        </Link>
      </div>
      <PromiseLoader condition={data !== null}>
        {() => (
          <>
            <FormattedTable
              configuration={{ columns: clone }}
              data={(data ?? []) as Asset[]}
            />
            <ButtonGroup>
              <Button
                icon={"left chevron"}
                disabled={pageStack.length === 0}
                onClick={() => {
                  if (pageStack.length === 0) return;
                  setPage(pageStack[pageStack.length - 1]);
                  setPageStack(pageStack.slice(0, pageStack.length - 1));
                }}
              />
              <Button
                icon={"right chevron"}
                disabled={data?.length === 0}
                onClick={() => {
                  if (data === null) return;
                  if (data?.length === 0) return;
                  setPageStack([...pageStack, page]);
                  setPage(data[data.length - 1].id);
                }}
              />
            </ButtonGroup>
          </>
        )}
      </PromiseLoader>
    </>
  );
};

export default requireAuth(
  bindWithDefaultLayout(Index, "Asset < Next Inventory Management")
);
