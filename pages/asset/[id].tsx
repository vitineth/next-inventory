import "semantic-ui-css/semantic.min.css";
import React, { useState } from "react";
import { Asset } from "../../@types/Asset";
import { GetServerSideProps } from "next";
import { getAsset } from "../../lib/dao/assets";
import SharedHeader from "../../components/SharedHeader";
import { TableSchema } from "../../components/FormattedTable";
import { defaultAssetSchema, getAssetSchema } from "../../lib/dao/settings";
import {
  Accordion,
  Button,
  Confirm,
  Container,
  Grid,
  Header,
  Icon,
  Label,
  Message,
  Segment,
} from "semantic-ui-react";
import RenderedProperty from "../../components/RenderedProperty";
import Link from "next/link";
import { QueryResponse } from "../../@types/APIResponse";
import TagSelector, { LibrarySelector } from "../../components/TagSelector";
import EditableProperty from "../../components/EditableProperty";
import { useRouter } from "next/router";
import bindWithDefaultLayout from "../../components/DefaultLayout";
import Head from "next/head";
import requireAuth from "../../utils/authentication";
import { FormattedMessage, useIntl } from "react-intl";

type AssetIDProps = {
  asset: Asset;
  schema: TableSchema;
};

export const getServerSideProps: GetServerSideProps<AssetIDProps> = async (
  context
) => {
  const [asset, schema] = await Promise.allSettled([
    await getAsset(context.query.id as string),
    getAssetSchema(),
  ]);

  if (asset.status === "rejected" || asset.value === null)
    return { notFound: true };

  const parsed =
    schema.status === "rejected"
      ? { columns: defaultAssetSchema }
      : schema.value;

  return {
    props: {
      asset: asset.value,
      schema: parsed,
    },
  };
};

const Id: React.FunctionComponent<AssetIDProps> = ({ asset, schema }) => {
  const intl = useIntl();
  const [state, setState] = useState(asset);
  const [error, setError] = useState<{ title: string; body: string } | null>(
    null
  );
  const [showTagAdd, setShowAddTag] = useState(false);
  const [showLibraryAdd, setShowAddLibrary] = useState(false);
  const [showDeleteConfirm, setShowDeleteConfirm] = useState(false);

  const router = useRouter();

  const patchProperty = (property: string, value: string | undefined) => {
    let modification: Record<string, any> = {};

    if (value === undefined) {
      modification[".unset"] = [property];
    } else {
      modification[property] = value;
    }

    fetch(`/api/asset/${state.identifier}`, {
      method: "PATCH",
      body: JSON.stringify(modification),
      headers: { "Content-Type": "application/json" },
    })
      .then((d) => d.json())
      .then((r: QueryResponse<any>) => {
        if (r.success) {
          setState((s) => ({
            ...s,
            [property]: value,
          }));
          setError(null);
        } else {
          setError({
            title: `${intl.formatMessage({
              defaultMessage: "Failed to assign",
            })} ${property}`,
            body: r.error,
          });
        }
      });
  };

  const modify = (
    entity: "tag" | "library",
    action: "remove" | "add",
    tag: string
  ) => {
    fetch(`/api/asset/${state.identifier}`, {
      method: "PATCH",
      body: JSON.stringify({
        [`.${entity}`]: {
          [`.${action}`]: [tag],
        },
      }),
      headers: { "Content-Type": "application/json" },
    })
      .then((d) => d.json())
      .then((r: QueryResponse<any>) => {
        if (r.success) {
          const key: "tags" | "libraries" =
            entity === "tag" ? "tags" : "libraries";

          setState((s) => ({
            ...s,
            [key]:
              action === "remove"
                ? s[key].filter((t) => t.name !== tag)
                : [...s[key], ...r.data[`.${entity}`][`.${action}`]],
          }));
          setError(null);
        } else {
          setError({
            title: `${intl.formatMessage({
              defaultMessage: "Failed to remove",
            })} ${entity}`,
            body: r.error,
          });
        }
      });
  };

  const { id, tags, libraries, ...rest } = asset;

  return (
    <>
      <Head>
        <title>
          {state.identifier} &lt;{" "}
          {intl.formatMessage({ defaultMessage: "Asset" })} &lt;{" "}
          {intl.formatMessage({ defaultMessage: "Next Inventory Management" })}
        </title>
      </Head>
      {error ? (
        <Message error header={error.title} content={error.body} />
      ) : undefined}

      <Header size="huge">{state.identifier}</Header>
      <Grid divided>
        <Grid.Row>
          <Grid.Column width={10}>
            {schema.columns.map((column) => (
              <EditableProperty
                onChange={(v) => patchProperty(column.access, v)}
                schema={column}
                value={state[column.access]}
                includeHeader={true}
                key={column.access}
              />
            ))}
          </Grid.Column>
          <Grid.Column width={6}>
            <Header size="large">
              <FormattedMessage defaultMessage={"Tags"} />
            </Header>
            <div>
              {state.tags.map((tag) => (
                <Label key={tag.id}>
                  <Link href={`/tag/${tag.name}`}>
                    <a>
                      <Icon name={tag.icon as any} />
                      {tag.name}
                    </a>
                  </Link>
                  <Icon
                    name="delete"
                    style={{ marginLeft: "10px" }}
                    onClick={() => modify("tag", "remove", tag.name)}
                  />
                </Label>
              ))}
            </div>
            <div style={{ marginTop: "10px" }}>
              {showTagAdd ? (
                <>
                  <TagSelector
                    supportAddition
                    active={[]}
                    exclude={state.tags.map((e) => e.name)}
                    onAdded={(t) => modify("tag", "add", t)}
                    onRemoved={(t) => {
                      if (state.tags.findIndex((e) => e.name === t) !== -1)
                        modify("tag", "remove", t);
                    }}
                  />
                  <Button onClick={() => setShowAddTag(false)}>
                    <FormattedMessage defaultMessage={"Done"} />
                  </Button>
                </>
              ) : (
                <span className="add-new" onClick={() => setShowAddTag(true)}>
                  <Icon name={"add circle"} />
                  <FormattedMessage defaultMessage={"Add new tag"} />
                </span>
              )}
            </div>

            <Header size="large">
              <FormattedMessage defaultMessage={"Libraries"} />
            </Header>
            <div>
              {state.libraries.map((tag) => (
                <Label key={tag.id}>
                  <Link href={`/tag/${tag.name}`}>
                    <a>{tag.name}</a>
                  </Link>
                  <Icon
                    name="delete"
                    style={{ marginLeft: "10px" }}
                    onClick={() => modify("library", "remove", tag.name)}
                  />
                </Label>
              ))}
            </div>
            <div style={{ marginTop: "10px" }}>
              {showLibraryAdd ? (
                <>
                  <LibrarySelector
                    active={[]}
                    exclude={state.libraries.map((e) => e.name)}
                    onAdded={(t) => modify("library", "add", t)}
                    onRemoved={(t) => {
                      if (state.libraries.findIndex((e) => e.name === t) !== -1)
                        modify("library", "remove", t);
                    }}
                  />
                  <Button onClick={() => setShowAddLibrary(false)}>
                    <FormattedMessage defaultMessage={"Done"} />
                  </Button>
                </>
              ) : (
                <span
                  className="add-new"
                  onClick={() => setShowAddLibrary(true)}
                >
                  <Icon name={"add circle"} />
                  <FormattedMessage defaultMessage={"Add new library"} />
                </span>
              )}
            </div>
            <Header size="large">
              <FormattedMessage defaultMessage={"Delete"} />
            </Header>
            <div>
              <Button color={"red"} onClick={() => setShowDeleteConfirm(true)}>
                <FormattedMessage defaultMessage={"Delete"} />
              </Button>
              <Confirm
                open={showDeleteConfirm}
                onCancel={() => setShowDeleteConfirm(false)}
                onConfirm={() => {
                  fetch(`/api/asset/${asset.identifier}`, {
                    method: "DELETE",
                  })
                    .then((r) => {
                      if (r.ok) return router.push("/asset");
                      throw new Error(
                        intl.formatMessage({
                          defaultMessage: "Failed to delete",
                        })
                      );
                    })
                    .catch(() => {
                      alert(
                        intl.formatMessage({
                          defaultMessage: "Failed to delete asset",
                        })
                      );
                    });
                }}
              />
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <details>
        <summary>
          <FormattedMessage defaultMessage={"Raw JSON Content"} />
        </summary>
        <Segment>
          <pre>{JSON.stringify(rest, null, 4)}</pre>
        </Segment>
      </details>
    </>
  );
};

export default requireAuth(bindWithDefaultLayout(Id));
