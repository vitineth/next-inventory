import "semantic-ui-css/semantic.min.css";
import { NextPage } from "next";
import { useEffect, useState } from "react";
import {
  Button,
  Dropdown,
  Form,
  Input,
  Loader,
  Message,
} from "semantic-ui-react";
import { Tag } from "../../@types/Tag";
import { Library } from "../../@types/Library";
import { TableSchema } from "../../components/FormattedTable";
import { CreateAsset } from "../../@types/Asset";
import IconDropdown from "../../components/IconDropdown";
import { ModificationResponse } from "../../@types/APIResponse";
import bindWithDefaultLayout from "../../components/DefaultLayout";
import requireAuth from "../../utils/authentication";
import { FormattedMessage, useIntl } from "react-intl";

const Create: NextPage = () => {
  const intl = useIntl();
  const [isLoading, setLoading] = useState(false);
  const [tags, setTags] = useState<Tag[] | null>(null);
  const [libraries, setLibraries] = useState<Library[] | null>(null);
  const [schema, setSchema] = useState<TableSchema | null>(null);
  const [error, setError] = useState<{ title: string; body: string } | null>(
    null
  );
  const [creation, setCreation] = useState<CreateAsset>({ identifier: "" });
  const [cachedIconHandler, setCachedIconHandler] = useState<
    Record<string, (v: string) => void>
  >({});

  useEffect(() => {
    if (!isLoading && tags === null && libraries === null) {
      setLoading(true);
      Promise.all([
        fetch("/api/tag")
          .then((d) => d.json())
          .then((d) => setTags(d.data)),
        fetch("/api/library")
          .then((d) => d.json())
          .then((d) => setLibraries(d.data)),
        fetch("/api/schema/asset")
          .then((d) => d.json())
          .then((d) => setSchema(d.data ?? { columns: [] })),
      ]).then(() => setLoading(false));
    }
  }, [isLoading, tags, libraries, schema]);

  if (isLoading) {
    return <Loader />;
  }

  let components = (schema?.columns ?? [])
    .filter((e) => typeof e.format === "string")
    .map((e) => {
      switch (e.format) {
        case "text":
          return (
            <Form.Input
              label={e.display}
              type="text"
              key={e.access}
              value={creation[e.access] ?? ""}
              onChange={(_, d) =>
                setCreation((p) => ({ ...p, [e.access]: d.value }))
              }
            />
          );
        case "code":
          return (
            <Form.Field key={e.access}>
              <label>{e.display}</label>
              <Form.Input
                value={creation[e.access] ?? ""}
                type="text"
                className="code force-input"
                onChange={(_, d) =>
                  setCreation((p) => ({ ...p, [e.access]: d.value }))
                }
              />
            </Form.Field>
          );
        case "link":
          return (
            <Form.Field key={e.access}>
              <label>{e.display}</label>
              <Input
                value={creation[e.access] ?? ""}
                type="text"
                icon="chain"
                iconPosition="left"
                onChange={(_, d) =>
                  setCreation((p) => ({ ...p, [e.access]: d.value }))
                }
              />
            </Form.Field>
          );
        case "image":
          return (
            <Form.Input
              label={e.display}
              type="file"
              key={e.access}
              accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*"
              onChange={(c) => {
                const files = c.target.files;
                if (files && files.length > 0) {
                  const reader = new FileReader();
                  reader.readAsDataURL(files[0]);
                  reader.onloadend = () => {
                    setCreation((p) => ({
                      ...p,
                      [e.access]: reader.result,
                    }));
                  };
                }
              }}
            />
          );
        case "icon":
          if (cachedIconHandler[e.access] === undefined)
            setCachedIconHandler((p) => ({
              ...p,
              [e.access]: (v: string) =>
                setCreation((p) => ({ ...p, [e.access]: v })),
            }));
          return (
            <Form.Field>
              <label>{e.display}</label>
              <IconDropdown
                value={creation[e.access]}
                onIconSelected={cachedIconHandler[e.access]}
              />
            </Form.Field>
          );
      }
    });

  const submit = () => {
    if (creation.identifier.trim() === "") {
      setError({
        title: intl.formatMessage({ defaultMessage: "Invalid submission" }),
        body: intl.formatMessage({
          defaultMessage: 'A value for "identifier" is required',
        }),
      });
      return;
    }

    fetch("/api/asset", {
      method: "POST",
      body: JSON.stringify(creation),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((r) => r.json())
      .then((d: ModificationResponse) => {
        if (d.success) {
          if (d.id) window.location.assign(`/asset/${d.id}`);
          else window.location.assign("/asset");
        } else {
          setError({
            title: intl.formatMessage({ defaultMessage: "Invalid submission" }),
            body: d.error,
          });
        }
      })
      .catch((e: Error) => {
        setError({
          title: intl.formatMessage({ defaultMessage: "Invalid submission" }),
          body: e.message,
        });
      });
  };

  return (
    <>
      {error !== null ? (
        <Message error={true} header={error.title} content={error.body} />
      ) : undefined}
      <Form>
        <Form.Input
          label={intl.formatMessage({ defaultMessage: "Identifier" })}
          type="text"
          required
          value={creation.identifier}
          onChange={(_, d) =>
            setCreation((p) => ({ ...p, identifier: d.value }))
          }
        />

        {components}

        <Form.Field>
          <label>
            <FormattedMessage defaultMessage={"Tags"} />
          </label>
          <Dropdown
            placeholder={intl.formatMessage({ defaultMessage: "Tags" })}
            fluid
            multiple
            search
            selection
            options={(tags ?? []).map((v) => ({
              text: v.name,
              value: v.name,
            }))}
            value={creation.tags ?? []}
            onChange={(_, i) =>
              setCreation((p) => ({ ...p, tags: i.value as string[] }))
            }
          />
        </Form.Field>

        <Form.Field>
          <label>
            <FormattedMessage defaultMessage={"Libraries"} />
          </label>
          <Dropdown
            placeholder={intl.formatMessage({ defaultMessage: "Libraries" })}
            fluid
            multiple
            search
            selection
            // allowAdditions
            options={(libraries ?? []).map((v) => ({
              text: v.name,
              value: v.name,
            }))}
            value={creation.tags ?? []}
            onChange={(_, i) =>
              setCreation((p) => ({
                ...p,
                libraries: i.value as string[],
              }))
            }
          />
        </Form.Field>

        <Button onClick={submit} primary>
          <FormattedMessage defaultMessage={"Create"} />
        </Button>
      </Form>
    </>
  );
};

export default requireAuth(
  bindWithDefaultLayout(Create, "Create < Asset < Next Inventory Management")
);
