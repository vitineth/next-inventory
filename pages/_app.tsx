import "../styles/globals.css";
import type { AppProps } from "next/app";
import { useRouter } from "next/router";
import cookie from "cookie";

import EnglishLang from "../content/compiled-locales/en.json";
import GermanLang from "../content/compiled-locales/de.json";
import { ReactElement, ReactNode, useMemo } from "react";
import { FormattedMessage, IntlProvider } from "react-intl";
import { HTML5Backend } from "react-dnd-html5-backend";
import { DndProvider } from "react-dnd";
import { NextPage } from "next";
import { IncomingMessage } from "http";
import { SessionProvider } from "next-auth/react";
import RequireAuthenticated from "../components/RequireAuthenticated";

const LANGUAGE_MAPPING: Record<string, any> = {
  de: GermanLang,
  en: EnglishLang,
};

export type PageWithLayout<T> = NextPage<T> & {
  getLayout?: (page: ReactElement) => ReactNode;
};

export type AppPropsWithLayout<T> = AppProps<T> & {
  Component: PageWithLayout<T>;
};

function MyApp({ Component, pageProps }: AppPropsWithLayout<any>) {
  const { locale } = useRouter();
  const { session } = pageProps;
  const [shortLocale] = locale ? locale.split("-") : ["en"];

  const messages = useMemo(
    () => LANGUAGE_MAPPING[shortLocale] ?? EnglishLang,
    [shortLocale]
  );
  const getLayout = Component.getLayout ?? ((page) => page);

  return (
    <SessionProvider session={session}>
      <IntlProvider
        locale={shortLocale}
        messages={messages}
        onError={console.error}
      >
        {getLayout(<Component {...pageProps} />)}
      </IntlProvider>
    </SessionProvider>
  );
}

export default MyApp;
