import "semantic-ui-css/semantic.min.css";
import {
  AssignLibraries,
  AssignTags,
  BulkImageUpload,
  CreateAssetForEach,
  CreateLibraryForEach,
  CreateTagForEach,
  CustomCode,
  FormForEach,
  NRecord,
  Pipeline,
  PipelineStage,
} from "../../../@types/Pipeline";
import { GetServerSideProps, NextPage } from "next";
import { getPipeline } from "../../../lib/dao/pipelines";
import SharedHeader from "../../../components/SharedHeader";
import {
  Button,
  Container,
  Form,
  Header,
  Icon,
  Input,
  Label,
  Message,
  Progress,
  Segment,
  Step,
} from "semantic-ui-react";
import React, { DragEvent, useEffect, useMemo, useRef, useState } from "react";
import PipelineStageElement from "../../../components/PipelineStageElement";
import SchemaEditElement from "../../../components/SchemaEditElement";
import sandbox from "../../../utils/sandbox";
import zod from "zod";
import { TagCreate } from "../../../@types/Tag";
import { LibraryCreate } from "../../../@types/Library";
import { CreateAsset } from "../../../@types/Asset";
import NonSSRWrapper from "../../../components/NonSSRWrapper";
import TagSelector, { LibrarySelector } from "../../../components/TagSelector";
import Link from "next/link";
import useEffectOnce from "../../../utils/use-effect-once";
import bindWithDefaultLayout from "../../../components/DefaultLayout";
import Head from "next/head";
import requireAuth from "../../../utils/authentication";
import { FormattedMessage, useIntl } from "react-intl";

type PipelineIDProps = {
  pipeline: Pipeline;
};

const saveTag = (tag: TagCreate) =>
  fetch("/api/tag", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(tag),
  })
    .then((r) => r.json())
    .then((j) => {
      if (!j.success) throw new Error(j.error);
      return { ...tag, id: j.id };
    });
const saveLibrary = (tag: LibraryCreate) =>
  fetch("/api/library", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(tag),
  })
    .then((r) => r.json())
    .then((j) => {
      if (!j.success) throw new Error(j.error);
      return { ...tag, id: j.id };
    });
const saveAsset = (tag: CreateAsset) =>
  fetch("/api/asset", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(tag),
  })
    .then((r) => r.json())
    .then((j) => {
      if (!j.success) throw new Error(j.error);
      return { ...tag, id: j.id };
    });

export const getServerSideProps: GetServerSideProps<PipelineIDProps> = async (
  context
) => {
  const pipeline = await getPipeline(context.query.id as string);

  if (pipeline === null)
    return {
      notFound: true,
    };

  return {
    props: {
      pipeline,
    },
  };
};

type ExecuteProps<T> = {
  dataset: any[];
  form: T;
  complete: (dataset: any[]) => void;
  error: (message: string) => void;
};

const ForEachWrapper = ({
  index,
  length,
  element,
  children,
}: {
  index: number;
  length: number;
  element: any;
  children?: React.ReactNode[];
}) => {
  const photoElement = useMemo(() => {
    const photoEntry = Object.entries(element).find((e) => e[0] === "photo");
    let photoElement = null;
    if (photoEntry) {
      return (
        <picture>
          <source srcSet={photoEntry[1] as string} type="image/png" />
          <img src={photoEntry[1] as string} alt="" />
        </picture>
      );
    }

    return null;
  }, [element]);

  return (
    <>
      <Progress progress indicating percent={(index / length) * 100} />

      <summary>
        <FormattedMessage defaultMessage={"Raw JSON Content"} />
      </summary>
      <Segment>
        <pre>
          {JSON.stringify(
            Object.fromEntries(
              Object.entries(element).map(([key, value]) => {
                if (typeof value !== "string") return [key, value];
                return [
                  key,
                  value.substring(0, 600) + (value.length > 600 ? "..." : ""),
                ];
              })
            ),
            null,
            4
          )}
        </pre>
        {photoElement}
      </Segment>

      {children}
    </>
  );
};

const ExecuteForm = ({
  dataset,
  form,
  complete,
}: ExecuteProps<FormForEach>) => {
  const intl = useIntl();
  const [index, setIndex] = useState(0);
  const [cachedIconHandler, setCachedIconHandler] = useState<
    Record<string, (v: string) => void>
  >({});
  const [morphedDataset, setMorphedDataset] = useState(dataset);

  if (dataset.length === 0) {
    complete(dataset);
    return null;
  }

  let components = (form.form ?? [])
    .filter((e) => typeof e.format === "string")
    .map((e, idx) => {
      if (e.format === "icon") {
        if (cachedIconHandler[e.access] === undefined)
          setCachedIconHandler((p) => ({
            ...p,
            [e.access]: (v: string) =>
              setMorphedDataset((d) => {
                const clone = [...d];
                clone[index][e.access] = v;
                return clone;
              }),
          }));
        return (
          <SchemaEditElement
            key={`editelement.${e.access}.${idx}`}
            property={e}
            onSet={cachedIconHandler[e.access]}
            value={morphedDataset[index][e.access]}
          />
        );
      } else {
        return (
          <SchemaEditElement
            key={`editelement.${e.access}.${idx}`}
            property={e}
            onSet={(v) =>
              setMorphedDataset((d) => {
                const clone = [...d];
                clone[index][e.access] = v;
                return clone;
              })
            }
            value={morphedDataset[index][e.access]}
          />
        );
      }
    });

  return (
    <>
      <ForEachWrapper
        index={index}
        length={dataset.length}
        element={dataset[index]}
      >
        <Form>{components}</Form>
        <Button
          onClick={() => {
            if (index === dataset.length - 1) {
              complete(morphedDataset);
            } else {
              setIndex((v) => v + 1);
            }
          }}
          primary
        >
          {index === dataset.length - 1
            ? intl.formatMessage({ defaultMessage: "Save" })
            : intl.formatMessage({ defaultMessage: "Move to next record" })}
        </Button>
      </ForEachWrapper>
    </>
  );
};

const ExecuteAssign = ({
  dataset,
  complete,
  mode,
}:
  | (ExecuteProps<AssignTags> & { mode: "tags" })
  | (ExecuteProps<AssignLibraries> & { mode: "libraries" })) => {
  const intl = useIntl();
  const [index, setIndex] = useState(0);
  const [morphedDataset, setMorphedDataset] = useState(dataset);
  const [active, setActive] = useState<string[]>([]);

  if (dataset.length === 0) {
    complete(dataset);
    return null;
  }

  const Element = mode === "tags" ? TagSelector : LibrarySelector;

  return (
    <>
      <ForEachWrapper
        index={index}
        length={dataset.length}
        element={dataset[index]}
      >
        <div>
          {active.map((tag) => (
            <Label key={tag}>
              <a>{tag}</a>
              <Icon
                name="delete"
                style={{ marginLeft: "10px" }}
                onClick={() => setActive((o) => o.filter((v) => v !== tag))}
              />
            </Label>
          ))}
        </div>

        <Element
          active={[]}
          supportAddition
          exclude={active}
          onAdded={(t) => setActive((a) => a.concat([t]))}
          onRemoved={(t) => setActive((a) => a.filter((e) => e !== t))}
        />

        <Button
          onClick={() => {
            const clone = [...morphedDataset];
            clone[index] = {
              ...clone[index],
              [mode]: [...active],
            };
            setMorphedDataset(clone);
            setActive([]);

            if (index === dataset.length - 1) {
              complete(clone);
            } else {
              setIndex((v) => v + 1);
            }
          }}
          primary
        >
          {index === dataset.length - 1
            ? intl.formatMessage({ defaultMessage: "Save" })
            : intl.formatMessage({ defaultMessage: "Move to next record" })}
        </Button>
      </ForEachWrapper>
    </>
  );
};

const ExecuteCustomCode = ({
  dataset,
  form,
  complete,
  error,
}: ExecuteProps<CustomCode>) => {
  const [launched, setLaunched] = useState(false);
  const intl = useIntl();

  useEffect(() => {
    if (typeof window === "undefined") return;
    if (launched) return;
    setLaunched(true);

    let interceptComplete = (dataset: any[]) => complete(dataset);

    sandbox(
      form.code,
      {
        $: {
          records: dataset,
        },
      },
      {}
    ).then((result) => {
      if (Array.isArray(result)) {
        interceptComplete(result);
      } else {
        error(
          intl.formatMessage({
            defaultMessage: "Failed to execute code - didn't return an array!",
          })
        );
        throw new Error(
          intl.formatMessage({
            defaultMessage: "Failed to execute code - didn't return an array!",
          })
        );
      }
    });

    return () => {
      interceptComplete = () => undefined;
    };
  }, [form, launched, complete, dataset, intl, error]);

  if (typeof window === "undefined")
    return (
      <Message
        error
        header={
          <FormattedMessage
            defaultMessage={
              "This component can only be rendered on the client side!"
            }
          />
        }
      />
    );

  return (
    <Message
      info
      header={<FormattedMessage defaultMessage={"Running code"} />}
      content={
        <FormattedMessage
          defaultMessage={
            "The code in this request is currently executing - please wait."
          }
        />
      }
    />
  );
};

const ExecuteCreateTag = ({
  dataset,
  form,
  complete,
  error,
}: ExecuteProps<CreateTagForEach>) => {
  const intl = useIntl();
  const result = zod
    .array(
      zod
        .object({
          name: zod.string(),
          icon: zod.string(),
        })
        .passthrough()
    )
    .safeParse(dataset);

  if (!result.success) {
    console.error(result);
    error(intl.formatMessage({ defaultMessage: "Invalid tag properties!" }));
    throw new Error(
      intl.formatMessage({ defaultMessage: "Invalid tag properties!" })
    );
  }

  const [launched, setLaunched] = useState(false);

  useEffectOnce(() => {
    if (launched) return;
    setLaunched(true);

    let interceptComplete = (dataset: any[]) => complete(dataset);

    Promise.allSettled(result.data.map((e) => saveTag(e))).then((result) => {
      if (
        result.filter((e) => e.status === "fulfilled").length === result.length
      ) {
        interceptComplete(result.map((e) => (e as any).value));
      } else {
        error(
          intl.formatMessage({ defaultMessage: "Failed to create new tags" })
        );
        throw new Error(
          intl.formatMessage({ defaultMessage: "Failed to create new tags" })
        );
      }
    });

    return () => {
      interceptComplete = () => undefined;
    };
  });

  return (
    <Message
      info
      header={<FormattedMessage defaultMessage={"Creating tags"} />}
      content={
        <FormattedMessage
          defaultMessage={"The tags are currently being created - please wait."}
        />
      }
    />
  );
};
const ExecuteCreateAsset = ({
  dataset,
  form,
  complete,
  error,
}: ExecuteProps<CreateAssetForEach>) => {
  const intl = useIntl();
  const result = zod
    .array(
      zod
        .object({
          identifier: zod.string(),
          tags: zod.array(zod.string()).optional(),
          libraries: zod.array(zod.string()).optional(),
        })
        .passthrough()
    )
    .safeParse(dataset);

  if (!result.success) {
    console.error(result);
    error(intl.formatMessage({ defaultMessage: "Invalid asset properties" }));
    throw new Error(
      intl.formatMessage({ defaultMessage: "Invalid asset properties" })
    );
  }

  const [launched, setLaunched] = useState(false);

  useEffectOnce(() => {
    if (launched) return;
    setLaunched(true);

    let interceptComplete = (dataset: any[]) => complete(dataset);

    Promise.allSettled(result.data.map((e) => saveAsset(e))).then((result) => {
      if (
        result.filter((e) => e.status === "fulfilled").length === result.length
      ) {
        interceptComplete(result.map((e) => (e as any).value));
      } else {
        error(
          intl.formatMessage({ defaultMessage: "Failed to create new assets" })
        );
        throw new Error(
          intl.formatMessage({ defaultMessage: "Failed to create new assets" })
        );
      }
    });

    return () => {
      interceptComplete = () => undefined;
    };
  });

  return (
    <Message
      info
      header={<FormattedMessage defaultMessage={"Creating assets"} />}
      content={
        <FormattedMessage
          defaultMessage={
            "The assets are currently being created - please wait."
          }
        />
      }
    />
  );
};
const ExecuteCreateLibrary = ({
  dataset,
  form,
  complete,
  error,
}: ExecuteProps<CreateLibraryForEach>) => {
  const intl = useIntl();
  const result = zod
    .array(
      zod
        .object({
          name: zod.string(),
        })
        .passthrough()
    )
    .safeParse(dataset);

  if (!result.success) {
    console.error(result);
    error(
      intl.formatMessage({ defaultMessage: "Invalid library properties!" })
    );
    throw new Error(
      intl.formatMessage({ defaultMessage: "Invalid library properties!" })
    );
  }

  const [launched, setLaunched] = useState(false);

  useEffectOnce(() => {
    if (launched) return;
    setLaunched(true);

    let interceptComplete = (dataset: any[]) => complete(dataset);

    Promise.allSettled(result.data.map((e) => saveLibrary(e))).then(
      (result) => {
        if (
          result.filter((e) => e.status === "fulfilled").length ===
          result.length
        ) {
          interceptComplete(result.map((e) => (e as any).value));
        } else {
          error(
            intl.formatMessage({
              defaultMessage: "Failed to create new libraries",
            })
          );
          throw new Error(
            intl.formatMessage({
              defaultMessage: "Failed to create new libraries",
            })
          );
        }
      }
    );

    return () => {
      interceptComplete = () => undefined;
    };
  });

  return (
    <Message
      info
      header={<FormattedMessage defaultMessage={"Creating assets"} />}
      content={
        <FormattedMessage
          defaultMessage={
            "The assets are currently being created - please wait."
          }
        />
      }
    />
  );
};
const ExecuteNRecord = ({ dataset, form, complete }: ExecuteProps<NRecord>) => {
  complete([
    ...dataset,
    ...Array(form.amount)
      .fill(0)
      .map(() => ({})),
  ]);
  return (
    <Message
      info
      header={<FormattedMessage defaultMessage={"Creating assets"} />}
      content={
        <FormattedMessage
          defaultMessage={
            "The assets are currently being created - please wait."
          }
        />
      }
    />
  );
};
const ExecuteBulkImageUpload = ({
  dataset,
  form,
  complete,
  error,
}: ExecuteProps<BulkImageUpload>) => {
  const ref = useRef<HTMLDivElement>(null);
  const intl = useIntl();

  const upload = (list: FileList) => {
    const images = Array.prototype.filter.call(list, (e) => {
      return e.type.startsWith("image/");
    });

    if (images.length !== list.length) {
      alert(
        intl.formatMessage({
          defaultMessage: "Not all files were images! Please try again",
        })
      );
      error(
        intl.formatMessage({
          defaultMessage: "Not all files were images! Please try again",
        })
      );
      return;
    }
    if (images.length === 0) {
      alert(intl.formatMessage({ defaultMessage: "No files matches" }));
      error(intl.formatMessage({ defaultMessage: "No files matches" }));
      return;
    }

    Promise.allSettled(
      images.map(
        (e) =>
          new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.onload = (ev) => {
              if (ev.target) resolve(ev.target.result);
              else
                reject(
                  new Error(
                    intl.formatMessage({ defaultMessage: "invalid result" })
                  )
                );
            };
            reader.onerror = (ev) => reject(ev);
            reader.readAsDataURL(e);
          })
      )
    ).then((results) => {
      const output: Record<string, any>[] = [];
      for (const entry of results) {
        if (entry.status === "rejected") {
          error(
            intl.formatMessage({ defaultMessage: "Failed to load all images" })
          );
          throw new Error(
            intl.formatMessage({ defaultMessage: "Failed to load all images" })
          );
        } else {
          output.push({ [form.key]: entry.value });
        }
      }

      complete(output);
    });
  };

  return (
    <div
      ref={ref}
      onDrop={(e: DragEvent<HTMLDivElement>) => {
        upload(e.dataTransfer.files);
        e.preventDefault();
        e.stopPropagation();
      }}
      onDragOver={(e) => {
        // TODO: for some reason onDrop isn't firing without this - might be a linux thing
        console.log(e);
        e.preventDefault();
        // e.stopPropagation();
      }}
    >
      <Segment placeholder>
        <Header icon>
          <Icon name="file outline" />
          <FormattedMessage defaultMessage={"Upload image files here"} />
        </Header>
        <Input
          type="file"
          multiple
          onChange={(event) => {
            if (event.target.files === null) return;
            upload(event.target.files);
          }}
        />
      </Segment>
    </div>
  );
  return (
    <Message
      info
      header={<FormattedMessage defaultMessage={"Creating assets"} />}
      content={
        <FormattedMessage
          defaultMessage={
            "The assets are currently being created - please wait."
          }
        />
      }
    />
  );
};

const StageToComponent = (
  props: { pipeline: PipelineStage } & Omit<ExecuteProps<any>, "form">
) => {
  switch (props.pipeline.type) {
    case "form":
      return (
        <ExecuteForm
          dataset={props.dataset}
          form={props.pipeline}
          complete={props.complete}
          error={props.error}
        />
      );
    case "upload":
      return (
        <ExecuteBulkImageUpload
          dataset={props.dataset}
          form={props.pipeline}
          complete={props.complete}
          error={props.error}
        />
      );
    case "code":
      return (
        <ExecuteCustomCode
          dataset={props.dataset}
          form={props.pipeline}
          complete={props.complete}
          error={props.error}
        />
      );
    case "spawn":
      return (
        <ExecuteNRecord
          dataset={props.dataset}
          form={props.pipeline}
          complete={props.complete}
          error={props.error}
        />
      );
    case "create.tag":
      return (
        <ExecuteCreateTag
          dataset={props.dataset}
          form={props.pipeline}
          complete={props.complete}
          error={props.error}
        />
      );
    case "create.asset":
      return (
        <ExecuteCreateAsset
          dataset={props.dataset}
          form={props.pipeline}
          complete={props.complete}
          error={props.error}
        />
      );
    case "create.library":
      return (
        <ExecuteCreateLibrary
          dataset={props.dataset}
          form={props.pipeline}
          complete={props.complete}
          error={props.error}
        />
      );
    case "assign.tags":
      return (
        <ExecuteAssign
          key={`assign.tags`}
          mode={"tags"}
          dataset={props.dataset}
          form={props.pipeline}
          complete={props.complete}
          error={props.error}
        />
      );
    case "assign.libraries":
      return (
        <ExecuteAssign
          key={`assign.libraries`}
          mode={"libraries"}
          dataset={props.dataset}
          form={props.pipeline}
          complete={props.complete}
          error={props.error}
        />
      );
    default:
      return (
        <Message
          error
          header={
            <FormattedMessage defaultMessage={"Invalid pipeline stage"} />
          }
          content={
            <FormattedMessage
              defaultMessage={"Not sure what to do with this type!"}
            />
          }
        />
      );
  }
};

const PipelinePage: NextPage<PipelineIDProps> = ({ pipeline }) => {
  const intl = useIntl();
  const [error, setError] = useState<{ title: string; body: string } | null>(
    null
  );
  const [stageIndex, setStageIndex] = useState(0);
  const [running, setRunning] = useState(typeof window !== "undefined");
  const [dataset, setDataset] = useState<any[]>([]);

  if (stageIndex >= pipeline.stages.length) {
    return (
      <>
        <SharedHeader />
        <Container>
          <Header size="huge">{pipeline.name}</Header>
          <Message
            success
            header={
              <FormattedMessage
                defaultMessage={"Pipeline completed successfully"}
              />
            }
            content={
              <FormattedMessage
                defaultMessage={
                  "Your pipeline succeeded, press the link below to return to the pipeline view page"
                }
              />
            }
          />
          <Link href={`/pipeline/${pipeline.name}`}>
            <FormattedMessage defaultMessage={"Return to view"} />
          </Link>
        </Container>
      </>
    );
  }

  return (
    <>
      <Head>
        <title>
          <FormattedMessage defaultMessage={"Execute"} /> &lt; {pipeline.name}{" "}
          &lt; <FormattedMessage defaultMessage={"Pipeline"} /> &lt;{" "}
          <FormattedMessage defaultMessage={"Next Inventory Management"} />
        </title>
      </Head>
      <Header size="huge">{pipeline.name}</Header>
      {error ? (
        <Message error header={error.title} content={error.body} />
      ) : undefined}
      <Header size="large">
        <FormattedMessage defaultMessage={"Sequence"} />
      </Header>
      <Step.Group vertical fluid>
        {pipeline.stages.map((stage, i) => (
          <PipelineStageElement
            state={
              stageIndex === i
                ? "active"
                : stageIndex > i
                ? "complete"
                : "pending"
            }
            {...stage}
            key={`${i}.${stage.type}`}
          />
        ))}
      </Step.Group>
      <NonSSRWrapper>
        {running ? (
          <StageToComponent
            pipeline={pipeline.stages[stageIndex]}
            dataset={dataset}
            complete={(d) => {
              setDataset(d);
              setStageIndex((i) => i + 1);
            }}
            error={(e) => {
              setError({
                title: intl.formatMessage({
                  defaultMessage: "Failed to run pipeline!",
                }),
                body: e,
              });
            }}
          />
        ) : undefined}
      </NonSSRWrapper>
    </>
  );
};

export default requireAuth(bindWithDefaultLayout(PipelinePage));
