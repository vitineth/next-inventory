import "semantic-ui-css/semantic.min.css";
import { GetServerSideProps, NextPage } from "next";
import SharedHeader from "../../components/SharedHeader";
import {
  Button,
  Confirm,
  Container,
  Form,
  Grid,
  Header,
  Icon,
  Message,
  Step,
  TextArea,
} from "semantic-ui-react";
import SortableList from "../../components/SortableList";
import {
  BulkImageUpload,
  CustomCode,
  FormForEach,
  NRecord,
  Pipeline,
  PipelineStage,
} from "../../@types/Pipeline";
import React, { useState } from "react";
import SchemaEditElement from "../../components/SchemaEditElement";
import { SchemaTable } from "../schema";
import { TableSchema } from "../../components/FormattedTable";
import AceEditor from "react-ace";
import { getTag } from "../../lib/dao/tags";
import {
  defaultAssetSchema,
  defaultLibrarySchema,
  getAssetSchema,
  getTagsSchema,
} from "../../lib/dao/settings";
import { Tag } from "../../@types/Tag";
import { getPipeline } from "../../lib/dao/pipelines";
import { QueryResponse } from "../../@types/APIResponse";
import { Router, useRouter } from "next/router";
import PipelineStageElement from "../../components/PipelineStageElement";
import bindWithDefaultLayout from "../../components/DefaultLayout";
import Head from "next/head";
import requireAuth from "../../utils/authentication";
import { FormattedMessage, IntlShape, useIntl } from "react-intl";

type PipelineIDProps = {
  pipeline: Pipeline;
};

export const getServerSideProps: GetServerSideProps<PipelineIDProps> = async (
  context
) => {
  const pipeline = await getPipeline(context.query.id as string);

  if (pipeline === null)
    return {
      notFound: true,
    };

  return {
    props: {
      pipeline,
    },
  };
};

// TODO: localise
const readableNames = (intl: IntlShape) => ({
  code: "Custom Code",
  form: "Data Collection",
  upload: "Bulk Image Upload",
  "create.library": "Create Libraries",
  "create.tag": "Create Tag",
  "create.asset": "Create Asset",
  spawn: "Spawn Entry Records",
  "assign.tags": "Assign Tags",
  "assign.libraries": "Assign Libraries",
});

const FormConfigure: React.FunctionComponent<
  FormForEach & { save: (stage: FormForEach) => void }
> = (props) => {
  return (
    <>
      <p>
        <FormattedMessage
          defaultMessage={
            "This form will be shown for all records which are currently in the pipeline. Use the configuration table below to configure the questions which will be asked and the values they will be stored in within the records."
          }
        />
      </p>
      <SchemaTable
        schema={{ columns: props.form } as TableSchema}
        readOnly={[]}
        setError={console.error}
        onSuccess={(p) =>
          props.save({
            form: p,
            type: "form",
          })
        }
      />
    </>
  );
};

const CodeConfigure: React.FunctionComponent<
  CustomCode & { save: (stage: CustomCode) => void }
> = (props) => {
  const [code, setCode] = useState(props.code);

  return (
    <Form>
      <p>
        <FormattedMessage
          defaultMessage={
            "This custom block of code will be executed on your device with the data currently in the pipeline. You will have access to the current records via"
          }
        />
        <code>$.records</code>,
        <FormattedMessage defaultMessage={"all available libraries via"} />
        <code>async $.libraries()</code>,
        <FormattedMessage defaultMessage={"tags via"} />
        <code>async $.tags()</code>,
        <FormattedMessage defaultMessage={"and assets via"} />
        <code>async $.assets()</code>.
        <FormattedMessage
          defaultMessage={
            "The code is run in a sandboxed environment so some features may not be available"
          }
        />
      </p>
      <TextArea
        onChange={(e, d) => setCode(String(d.value ?? ""))}
        value={code}
        style={{ fontFamily: "JetBrains Mono, monospace" }}
        fluid
      />
      <div style={{ height: "20px" }} />
      <Button
        primary
        floated="right"
        onClick={() =>
          props.save({
            code,
            type: "code",
          })
        }
      >
        <FormattedMessage defaultMessage={"Save"} />
      </Button>
      <Button secondary floated="right" onClick={() => setCode(props.code)}>
        <FormattedMessage defaultMessage={"Cancel"} />
      </Button>
    </Form>
  );
};

const UploadConfigure: React.FunctionComponent<
  BulkImageUpload & { save: (stage: BulkImageUpload) => void }
> = (props) => {
  const intl = useIntl();
  const [maxSize, setMaxSize] = useState(props.sizeMax);
  const [key, setKey] = useState("photo");
  return (
    <Form>
      <p>
        <FormattedMessage
          defaultMessage={
            "This will display a file upload through which the user can upload any number of photos. A record will be created for each photo which can then be processed through the other pipeline stages. The maximum size below can be used to restrict the size of images uploaded"
          }
        />
      </p>
      <Form.Input
        label={intl.formatMessage({ defaultMessage: "Maximum Size (MB)" })}
        type="number"
        onChange={(_, d) => setMaxSize(Number(d.value))}
        value={maxSize}
        placeholder={intl.formatMessage({ defaultMessage: "Maximum Size" })}
      />
      <Form.Input
        label={intl.formatMessage({ defaultMessage: "Attribute name" })}
        type="text"
        onChange={(_, d) => setKey(d.value)}
        value={key}
        placeholder={intl.formatMessage({ defaultMessage: "Attribute name" })}
      />

      <div style={{ height: "20px" }} />
      <Button
        primary
        floated="right"
        onClick={() =>
          props.save({
            sizeMax: maxSize,
            type: "upload",
            key,
          })
        }
      >
        <FormattedMessage defaultMessage={"Save"} />
      </Button>
      <Button
        secondary
        floated="right"
        onClick={() => setMaxSize(props.sizeMax)}
      >
        <FormattedMessage defaultMessage={"Cancel"} />
      </Button>
    </Form>
  );
};

const SpawnConfigure: React.FunctionComponent<
  NRecord & { save: (stage: NRecord) => void }
> = (stage) => {
  const intl = useIntl();
  const [amount, setAmount] = useState(stage.amount);
  return (
    <Form>
      <p>
        <FormattedMessage
          defaultMessage={
            "This will create the given number of empty records which can be processed in the next stages. If used mid way through a pipeline it will inject blank records at the end"
          }
        />
      </p>
      <Form.Input
        label={intl.formatMessage({ defaultMessage: "Amount of records" })}
        type="number"
        onChange={(_, d) => setAmount(Number(d.value))}
        value={amount}
        placeholder={intl.formatMessage({ defaultMessage: "Maximum Size" })}
      />

      <div style={{ height: "20px" }} />
      <Button
        primary
        floated="right"
        onClick={() =>
          stage.save({
            amount,
            type: "spawn",
          })
        }
      >
        <FormattedMessage defaultMessage={"Save"} />
      </Button>
      <Button secondary floated="right" onClick={() => setAmount(stage.amount)}>
        <FormattedMessage defaultMessage={"Cancel"} />
      </Button>
    </Form>
  );
};

const Configure: React.FunctionComponent<
  PipelineStage & {
    save: (stage: PipelineStage) => void;
  }
> = (stage) => {
  const intl = useIntl();
  if (stage.type === "form") return <FormConfigure {...stage} />;
  if (stage.type === "code") return <CodeConfigure {...stage} />;
  if (stage.type === "upload") return <UploadConfigure {...stage} />;
  if (stage.type === "spawn") return <SpawnConfigure {...stage} />;
  if (stage.type === "create.library")
    return (
      <p>
        <FormattedMessage
          defaultMessage={
            "Creates a library entry from the record if valid. This will require the record to contain key"
          }
        />
        <code>name</code>
        <FormattedMessage defaultMessage={"otherwise this stage will fail."} />
      </p>
    );
  if (stage.type === "create.tag")
    return (
      <p>
        <FormattedMessage
          defaultMessage={
            "Creates a tag entry from the record if valid. This will require the record to contain keys"
          }
        />
        <code>icon</code>
        <FormattedMessage defaultMessage={"and"} />
        <code>name</code>
        <FormattedMessage
          defaultMessage={
            "otherwise this stage will fail. The icon value must also be within the"
          }
        />{" "}
        <a href="https://react.semantic-ui.com/elements/icon/">
          <FormattedMessage defaultMessage={"valid icon set."} />
        </a>
      </p>
    );
  if (stage.type === "create.asset")
    return (
      <p>
        <FormattedMessage
          defaultMessage={
            "Creates an asset entry from the record if valid. This will require the record to contain key {identifier} otherwise this stage will fail. You can specify the {tags} and {libraries} properties containing names of each to automatically resolve and link them"
          }
          values={{
            identifier: `<code>identifier</code>`,
            tags: `<code>tags</code>`,
            libraries: `<code>libraries</code>`,
          }}
        />
      </p>
    );
  return (
    <>
      <FormattedMessage defaultMessage={"not yet implemented"} />
    </>
  );
};

const PipelinePage: NextPage<PipelineIDProps> = (props) => {
  const [pipeline, setPipeline] = useState<Pipeline>(props.pipeline);

  const intl = useIntl();
  const router = useRouter();
  const [error, setError] = useState<{ title: string; body: string } | null>(
    null
  );
  const [activeStage, setActiveStage] = useState<number | null>(null);
  const [showDeleteConfirm, setShowDeleteConfirm] = useState(false);

  const items = pipeline.stages.map((e, i) => ({
    content: readableNames(intl)[e.type],
    onClick: () => setActiveStage(i),
  }));

  return (
    <>
      <Head>
        <title>
          {pipeline.name} &lt; <FormattedMessage defaultMessage={"Pipeline"} />{" "}
          &lt; <FormattedMessage defaultMessage={"Next Inventory Management"} />
        </title>
      </Head>
      <Confirm
        open={showDeleteConfirm}
        onCancel={() => setShowDeleteConfirm(false)}
        onConfirm={() => {
          fetch(`/api/pipeline/${pipeline.name}`, {
            method: "DELETE",
          })
            .then((r) => {
              if (r.ok) return router.push("/pipeline");
              throw new Error(
                intl.formatMessage({ defaultMessage: "Failed to delete" })
              );
            })
            .catch(() => {
              alert(
                intl.formatMessage({
                  defaultMessage: "Failed to delete pipeline",
                })
              );
            });
        }}
      />

      {error ? (
        <Message error header={error.title} content={error.body} />
      ) : undefined}
      <Header size="large">
        <FormattedMessage defaultMessage={"Overview"} />
        <Button.Group floated="right">
          <Button
            primary
            onClick={() => {
              fetch(`/api/pipeline/${props.pipeline?.name}`, {
                method: "PATCH",
                body: JSON.stringify({
                  ...pipeline,
                  name: props.pipeline?.name,
                }),
                headers: { "Content-Type": "application/json" },
              })
                .then((d) => d.json())
                .then((r: QueryResponse<any>) => {
                  if (!r.success) {
                    setError({
                      title: intl.formatMessage({
                        defaultMessage: `Failed to update pipeline`,
                      }),
                      body: r.error,
                    });
                  } else {
                    setError(null);
                  }
                });
            }}
          >
            <FormattedMessage defaultMessage={"Save"} />
          </Button>
          <Button
            secondary
            onClick={() => {
              router.push(`/pipeline/execute/${props.pipeline.name}`);
            }}
          >
            <FormattedMessage defaultMessage={"Run"} />
          </Button>
          <Button
            onClick={() => {
              setPipeline(props.pipeline);
            }}
          >
            <FormattedMessage defaultMessage={"Cancel"} />
          </Button>
          <Button
            color="red"
            onClick={() => {
              setShowDeleteConfirm(true);
            }}
          >
            <FormattedMessage defaultMessage={"Delete"} />
          </Button>
        </Button.Group>
      </Header>
      <Step.Group vertical fluid>
        {pipeline.stages.map((stage, i) => (
          <PipelineStageElement
            state="pending"
            {...stage}
            key={`${i}.${stage.type}`}
          />
        ))}
      </Step.Group>
      <Header size="large">
        <FormattedMessage defaultMessage={"Configure"} />
      </Header>
      <Grid divided>
        <Grid.Row>
          <Grid.Column width={4}>
            <Header size="medium">
              <FormattedMessage defaultMessage={"Current Stages"} />
            </Header>
            <SortableList
              key={`${items.length}.${activeStage}`}
              items={items}
              active={activeStage ?? -1}
            />
            <Header size="medium">
              <FormattedMessage defaultMessage={"Add New Stage"} />
            </Header>
            <Button
              fluid
              icon="leaf"
              labelPosition="left"
              content={intl.formatMessage({ defaultMessage: "Create Asset" })}
              className="space-lower"
              onClick={() =>
                setPipeline((o) => ({
                  ...o,
                  stages: [...o.stages, { type: "create.asset" }],
                }))
              }
            />
            <Button
              fluid
              icon="cubes"
              labelPosition="left"
              content={intl.formatMessage({ defaultMessage: "Create Library" })}
              className="space-lower"
              onClick={() =>
                setPipeline((o) => ({
                  ...o,
                  stages: [...o.stages, { type: "create.library" }],
                }))
              }
            />
            <Button
              fluid
              icon="tags"
              labelPosition="left"
              content={intl.formatMessage({ defaultMessage: "Create Tag" })}
              className="space-lower"
              onClick={() =>
                setPipeline((o) => ({
                  ...o,
                  stages: [...o.stages, { type: "create.tag" }],
                }))
              }
            />
            <Button
              fluid
              icon="code"
              labelPosition="left"
              content={intl.formatMessage({ defaultMessage: "Custom Code" })}
              className="space-lower"
              onClick={() =>
                setPipeline((o) => ({
                  ...o,
                  stages: [...o.stages, { type: "code", code: "" }],
                }))
              }
            />
            <Button
              fluid
              icon="file"
              labelPosition="left"
              content={intl.formatMessage({
                defaultMessage: "Data Collection",
              })}
              className="space-lower"
              onClick={() =>
                setPipeline((o) => ({
                  ...o,
                  stages: [...o.stages, { type: "form", form: [] }],
                }))
              }
            />
            <Button
              fluid
              icon="upload"
              labelPosition="left"
              content={intl.formatMessage({
                defaultMessage: "Bulk Image Upload",
              })}
              className="space-lower"
              onClick={() =>
                setPipeline((o) => ({
                  ...o,
                  stages: [
                    ...o.stages,
                    { type: "upload", sizeMax: 20, key: "photo" },
                  ],
                }))
              }
            />
            <Button
              fluid
              icon="grid layout"
              labelPosition="left"
              content={intl.formatMessage({
                defaultMessage: "Create Blank Records",
              })}
              className="space-lower"
              onClick={() =>
                setPipeline((o) => ({
                  ...o,
                  stages: [...o.stages, { type: "spawn", amount: 0 }],
                }))
              }
            />
            <Button
              fluid
              icon="tags"
              labelPosition="left"
              content={intl.formatMessage({ defaultMessage: "Assign Tags" })}
              className="space-lower"
              onClick={() =>
                setPipeline((o) => ({
                  ...o,
                  stages: [...o.stages, { type: "assign.tags" }],
                }))
              }
            />
            <Button
              fluid
              icon="boxes"
              labelPosition="left"
              content={intl.formatMessage({
                defaultMessage: "Assign Libraries",
              })}
              className="space-lower"
              onClick={() =>
                setPipeline((o) => ({
                  ...o,
                  stages: [...o.stages, { type: "assign.libraries" }],
                }))
              }
            />
          </Grid.Column>
          <Grid.Column width={12}>
            <Header size="medium">
              <FormattedMessage defaultMessage={"Configuration"} />
            </Header>
            {activeStage === -1 || activeStage === null ? undefined : (
              <Configure
                {...pipeline.stages[activeStage]}
                save={(stage: PipelineStage) => {
                  setPipeline((prev) => ({
                    ...prev,
                    stages: [
                      ...prev.stages.slice(0, activeStage),
                      stage,
                      ...prev.stages.slice(activeStage + 1),
                    ],
                  }));
                }}
              />
            )}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </>
  );
};

export default requireAuth(bindWithDefaultLayout(PipelinePage));
