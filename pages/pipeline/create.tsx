import "semantic-ui-css/semantic.min.css";
import { NextPage } from "next";
import { useEffect, useState } from "react";
import SharedHeader from "../../components/SharedHeader";
import { Button, Container, Form, Loader, Message } from "semantic-ui-react";
import { TableSchema } from "../../components/FormattedTable";
import { ModificationResponse } from "../../@types/APIResponse";
import SchemaEditElement from "../../components/SchemaEditElement";
import { LibraryCreate } from "../../@types/Library";
import bindWithDefaultLayout from "../../components/DefaultLayout";
import requireAuth from "../../utils/authentication";
import { FormattedMessage, useIntl } from "react-intl";

const Create: NextPage = () => {
  const intl = useIntl();
  const [isLoading, setLoading] = useState(false);
  const [error, setError] = useState<{ title: string; body: string } | null>(
    null
  );
  const [name, setName] = useState("");

  const submit = () => {
    if (name.trim() === "") {
      setError({
        title: intl.formatMessage({ defaultMessage: "Invalid submission" }),
        body: intl.formatMessage({
          defaultMessage: 'A value for "Name" is required',
        }),
      });
      return;
    }

    fetch("/api/pipeline", {
      method: "POST",
      body: JSON.stringify({
        name,
        stages: [],
      }),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((r) => r.json())
      .then((d: ModificationResponse) => {
        if (d.success) {
          if (d.id) window.location.assign(`/pipeline/${d.id}`);
          else window.location.assign("/pipeline");
        } else {
          setError({
            title: intl.formatMessage({ defaultMessage: "Invalid submission" }),
            body: d.error,
          });
        }
      })
      .catch((e: Error) => {
        setError({
          title: intl.formatMessage({ defaultMessage: "Invalid submission" }),
          body: e.message,
        });
      });
  };

  return (
    <>
      {error !== null ? (
        <Message error={true} header={error.title} content={error.body} />
      ) : undefined}
      <Form>
        <Form.Input
          label={intl.formatMessage({ defaultMessage: "Name" })}
          type="text"
          required
          value={name}
          onChange={(_, d) => setName(d.value)}
        />

        <Button onClick={submit} primary>
          <FormattedMessage defaultMessage={"Create"} />
        </Button>
      </Form>
    </>
  );
};

export default requireAuth(
  bindWithDefaultLayout(Create, "Create < Pipeline < Next Inventory Management")
);
