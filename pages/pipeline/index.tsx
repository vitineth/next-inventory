import { GetServerSideProps, NextPage } from "next";
import SharedHeader from "../../components/SharedHeader";
import { Container, Header, Icon } from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";
import FormattedTable, { TableSchema } from "../../components/FormattedTable";
import { Pipeline } from "../../@types/Pipeline";
import { getAllPipelines } from "../../lib/dao/pipelines";
import Link from "next/link";
import bindWithDefaultLayout from "../../components/DefaultLayout";
import requireAuth from "../../utils/authentication";
import { FormattedMessage } from "react-intl";

type PipelinePropType = {
  pipelines: Pipeline[];
};

export const getServerSideProps: GetServerSideProps<PipelinePropType> = async (
  context
) => {
  const [pipelines] = await Promise.all([getAllPipelines()]);
  return {
    props: {
      // schema,
      pipelines,
    },
  };
};

const Index: NextPage<PipelinePropType> = ({ pipelines }) => {
  // pipelines[0].
  return (
    <>
      <Header size="huge">
        <FormattedMessage defaultMessage={"Pipelines"} />
      </Header>
      <FormattedTable
        configuration={{
          columns: [
            {
              format: (n: string) => (
                <span>
                  <Link href={`/pipeline/${n}`}>
                    <a>
                      <Icon name="chain" />
                      {n}
                    </a>
                  </Link>
                </span>
              ),
              access: "name",
              display: "Name",
            },
          ],
        }}
        data={pipelines}
      />
    </>
  );
};

export default requireAuth(
  bindWithDefaultLayout(Index, "Pipeline < Next Inventory Management")
);
