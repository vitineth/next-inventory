import zod from "zod";
import { NextApiRequest, NextApiResponse } from "next";
import {
  ModificationResponse,
  QueryResponse,
} from "../../../@types/APIResponse";
import { constants } from "http2";
import { ShallowLibrary } from "../../../@types/Library";
import { createLibrary, getAllLibraries } from "../../../lib/dao/libraries";

const LibraryCreationValidator = zod
  .object({
    name: zod.string(),
  })
  .passthrough()
  .refine(
    (d) => !("id" in d) && !("asset" in d) && !(".unset" in d),
    "Forbidden keys present"
  );

async function create(
  req: NextApiRequest,
  res: NextApiResponse<ModificationResponse>
) {
  if (req.method !== "POST")
    return res.status(constants.HTTP_STATUS_METHOD_NOT_ALLOWED).end();

  const parse = LibraryCreationValidator.safeParse(req.body);
  if (!parse.success) {
    const flattened = parse.error.flatten();
    const error =
      flattened.formErrors.join("; ") +
      Object.entries(flattened.fieldErrors)
        .map(([k, v]) => `${k}: ${v.join(", ")}`)
        .join("; ");
    return res.status(constants.HTTP_STATUS_BAD_REQUEST).send({
      success: false,
      error,
    });
  }

  try {
    await createLibrary(parse.data);
    res.status(constants.HTTP_STATUS_OK).json({ success: true });
  } catch (e: any) {
    if ("code" in e && e.code === 11000) {
      return res.status(constants.HTTP_STATUS_BAD_REQUEST).json({
        success: false,
        error: `library ${e.keyValue.name} already exists`,
      });
    }

    console.error(e);
    res
      .status(constants.HTTP_STATUS_INTERNAL_SERVER_ERROR)
      .json({ success: false, error: "an internal server error occurred" });
  }
}

async function get(
  req: NextApiRequest,
  res: NextApiResponse<QueryResponse<ShallowLibrary[]>>
) {
  try {
    const libraries = await getAllLibraries();

    res
      .status(constants.HTTP_STATUS_OK)
      .json({ success: true, data: libraries as any });
  } catch (e) {
    console.error(e);
    res
      .status(constants.HTTP_STATUS_INTERNAL_SERVER_ERROR)
      .json({ success: false, error: "an internal server error occurred" });
  }
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  switch (req.method) {
    case "GET":
      return get(req, res);
    case "POST":
      return create(req, res);
    default:
      res.status(constants.HTTP_STATUS_METHOD_NOT_ALLOWED).end();
  }
}
