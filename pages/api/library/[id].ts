import { NextApiRequest, NextApiResponse } from "next";
import {
  ModificationResponse,
  QueryResponse,
} from "../../../@types/APIResponse";
import { constants } from "http2";
import zod from "zod";
import { Library } from "../../../@types/Library";
import {
  deleteLibrary,
  getLibrary,
  updateLibrary,
} from "../../../lib/dao/libraries";

const LibraryUpdateValidator = zod
  .object({
    ".unset": zod.array(zod.string()).optional(),
  })
  .passthrough()
  .refine((d) => !("id" in d) && !("asset" in d), "Forbidden keys present");

async function get(
  req: NextApiRequest,
  res: NextApiResponse<QueryResponse<Library>>,
  id: string
) {
  try {
    const search = await getLibrary(id);

    if (search === null) {
      return res.status(constants.HTTP_STATUS_NOT_FOUND).end();
    }

    res.status(constants.HTTP_STATUS_OK).json({
      success: true,
      data: search,
    });
  } catch (e: any) {
    console.error(e);
    res.status(constants.HTTP_STATUS_INTERNAL_SERVER_ERROR).json({
      success: false,
      error: "an internal server error occurred while querying",
    });
  }
}

async function patch(
  req: NextApiRequest,
  res: NextApiResponse<ModificationResponse>,
  id: string
) {
  const parse = LibraryUpdateValidator.safeParse(req.body);
  if (!parse.success) {
    const flattened = parse.error.flatten();
    const error =
      flattened.formErrors.join("; ") +
      Object.entries(flattened.fieldErrors)
        .map(([k, v]) => `${k}: ${v.join(", ")}`)
        .join("; ");
    return res.status(constants.HTTP_STATUS_BAD_REQUEST).send({
      success: false,
      error,
    });
  }

  try {
    const results = (await updateLibrary(id, parse.data)).modifiedCount === 1;
    if (!results) {
      return res.status(constants.HTTP_STATUS_NOT_FOUND).end();
    }

    res.status(constants.HTTP_STATUS_OK).json({ success: true });
  } catch (e: any) {
    console.error(e);
    res
      .status(constants.HTTP_STATUS_INTERNAL_SERVER_ERROR)
      .json({ success: false, error: "an internal server error occurred" });
  }
}

async function remove(req: NextApiRequest, res: NextApiResponse, id: string) {
  try {
    const results = (await deleteLibrary(id)).deletedCount === 1;
    if (!results) {
      return res.status(constants.HTTP_STATUS_NOT_FOUND).end();
    }

    res.status(constants.HTTP_STATUS_OK).json({ success: true });
  } catch (e: any) {
    console.error(e);
    res
      .status(constants.HTTP_STATUS_INTERNAL_SERVER_ERROR)
      .json({ success: false, error: "an internal server error occurred" });
  }
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<QueryResponse<Library> | ModificationResponse | void>
) {
  const { id } = req.query;

  if (typeof id !== "string") {
    return res.status(constants.HTTP_STATUS_NOT_FOUND).end();
  }

  switch (req.method) {
    case "GET":
      return get(req, res, id);
    case "PATCH":
      return patch(req, res, id);
    case "DELETE":
      return remove(req, res, id);
    default:
      res.status(constants.HTTP_STATUS_METHOD_NOT_ALLOWED).end();
  }
}
