import NextAuth from "next-auth";
import Keycloak from "next-auth/providers/keycloak";

export default NextAuth({
  secret: process.env.SECRET,
  providers: [
    Keycloak({
      clientId: process.env.KEYCLOAK_CLIENT_ID as string,
      clientSecret: process.env.KEYCLOAK_CLIENT_SECRET as string,
      issuer: process.env.KEYCLOAK_ISSUER as string,
    }),
  ],
});
