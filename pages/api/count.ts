import { NextApiRequest, NextApiResponse } from "next";
import { getAssetCount } from "../../lib/dao/assets";
import { getLibraryCount } from "../../lib/dao/libraries";
import { getTagCount } from "../../lib/dao/tags";

export type CountAPIData = {
  tags?: number;
  assets?: number;
  libraries?: number;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<CountAPIData>
) {
  let promises: Promise<
    Partial<Record<"tags" | "assets" | "libraries", number>>
  >[];
  if (Object.prototype.hasOwnProperty.call(req.query, "only")) {
    const options =
      typeof req.query.only === "string"
        ? req.query.only.split(",")
        : req.query.only;
    promises = [];
    if (options.includes("tags"))
      promises.push(getTagCount().then((v) => ({ tags: v })));
    if (options.includes("assets"))
      promises.push(getAssetCount().then((v) => ({ assets: v })));
    if (options.includes("libraries"))
      promises.push(getLibraryCount().then((v) => ({ libraries: v })));
  } else {
    promises = [
      getTagCount().then((v) => ({ tags: v })),
      getAssetCount().then((v) => ({ assets: v })),
      getLibraryCount().then((v) => ({ libraries: v })),
    ];
  }

  const result = (await Promise.all(promises)).reduce(
    (a, b) => ({ ...a, ...b }),
    {}
  );
  res.status(200).json(result);
}
