import { NextApiRequest, NextApiResponse } from "next";
import {
  ModificationResponse,
  QueryResponse,
} from "../../../@types/APIResponse";
import { constants } from "http2";
import { Asset, AssetUpdateValidator } from "../../../@types/Asset";
import { deleteAsset, getAsset, updateAsset } from "../../../lib/dao/assets";

async function get(
  req: NextApiRequest,
  res: NextApiResponse<QueryResponse<Asset>>,
  id: string
) {
  try {
    const search = await getAsset(id);

    if (search === null) {
      return res.status(constants.HTTP_STATUS_NOT_FOUND).end();
    }

    res.status(constants.HTTP_STATUS_OK).json({
      success: true,
      data: search,
    });
  } catch (e: any) {
    console.error(e);
    res.status(constants.HTTP_STATUS_INTERNAL_SERVER_ERROR).json({
      success: false,
      error: "an internal server error occurred while querying",
    });
  }
}

async function patch(
  req: NextApiRequest,
  res: NextApiResponse<any>,
  id: string
) {
  const parse = AssetUpdateValidator.safeParse(req.body);
  if (!parse.success) {
    const flattened = parse.error.flatten();
    const error =
      flattened.formErrors.join("; ") +
      Object.entries(flattened.fieldErrors)
        .map(([k, v]) => `${k}: ${v.join(", ")}`)
        .join("; ");
    return res.status(constants.HTTP_STATUS_BAD_REQUEST).send({
      success: false,
      error,
    });
  }

  try {
    const results = await updateAsset(id, parse.data);

    if (results.modified !== 1) {
      return res.status(constants.HTTP_STATUS_NOT_FOUND).end();
    }

    let tags: Record<string, any> = {};
    let libraries: Record<string, any> = {};
    if (results.addedTags.length > 0) tags[".add"] = results.addedTags;
    if (results.removedTags.length > 0) tags[".remove"] = results.removedTags;
    if (results.addedLibraries.length > 0)
      libraries[".add"] = results.addedLibraries;
    if (results.removedLibraries.length > 0)
      libraries[".remove"] = results.removedLibraries;

    res.status(constants.HTTP_STATUS_OK).json({
      success: true,
      data: {
        ...(Object.keys(tags).length > 0 ? { ".tag": tags } : undefined),
        ...(Object.keys(libraries).length > 0
          ? { ".library": libraries }
          : undefined),
      },
    });
  } catch (e: any) {
    console.error(e);
    res
      .status(constants.HTTP_STATUS_INTERNAL_SERVER_ERROR)
      .json({ success: false, error: "an internal server error occurred" });
  }
}

async function remove(req: NextApiRequest, res: NextApiResponse, id: string) {
  try {
    const results = (await deleteAsset(id)).deletedCount === 1;
    if (!results) {
      return res.status(constants.HTTP_STATUS_NOT_FOUND).end();
    }

    res.status(constants.HTTP_STATUS_OK).json({ success: true });
  } catch (e: any) {
    console.error(e);
    res
      .status(constants.HTTP_STATUS_INTERNAL_SERVER_ERROR)
      .json({ success: false, error: "an internal server error occurred" });
  }
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<QueryResponse<Asset> | ModificationResponse | void>
) {
  const { id } = req.query;

  if (typeof id !== "string") {
    return res.status(constants.HTTP_STATUS_NOT_FOUND).end();
  }

  switch (req.method) {
    case "GET":
      return get(req, res, id);
    case "PATCH":
      return patch(req, res, id);
    case "DELETE":
      return remove(req, res, id);
    default:
      res.status(constants.HTTP_STATUS_METHOD_NOT_ALLOWED).end();
  }
}
