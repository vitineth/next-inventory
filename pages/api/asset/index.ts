import zod from "zod";
import { NextApiRequest, NextApiResponse } from "next";
import {
  ModificationResponse,
  QueryResponse,
} from "../../../@types/APIResponse";
import { constants } from "http2";
import { ShallowLibrary } from "../../../@types/Library";
import { createLibrary, getAllLibraries } from "../../../lib/dao/libraries";
import {
  AssetQuery,
  compressAssets,
  createAsset,
  getAllAssets,
  queryAsset,
} from "../../../lib/dao/assets";
import { Asset } from "../../../@types/Asset";

const AssetCreationValidator = zod
  .object({
    identifier: zod.string(),
    tags: zod.array(zod.string()).optional(),
    libraries: zod.array(zod.string()).optional(),
  })
  .passthrough()
  .refine(
    (d) => !("id" in d) && !("asset" in d) && !(".unset" in d),
    "Forbidden keys present"
  );

export type CreateAsset = zod.infer<typeof AssetCreationValidator>;

async function create(
  req: NextApiRequest,
  res: NextApiResponse<ModificationResponse>
) {
  if (req.method !== "POST")
    return res.status(constants.HTTP_STATUS_METHOD_NOT_ALLOWED).end();

  const parse = AssetCreationValidator.safeParse(req.body);
  if (!parse.success) {
    const flattened = parse.error.flatten();
    const error =
      flattened.formErrors.join("; ") +
      Object.entries(flattened.fieldErrors)
        .map(([k, v]) => `${k}: ${v.join(", ")}`)
        .join("; ");
    return res.status(constants.HTTP_STATUS_BAD_REQUEST).send({
      success: false,
      error,
    });
  }

  try {
    await createAsset(parse.data);
    res.status(constants.HTTP_STATUS_OK).json({ success: true });
  } catch (e: any) {
    if ("code" in e && e.code === 11000) {
      return res.status(constants.HTTP_STATUS_BAD_REQUEST).json({
        success: false,
        error: `library ${e.keyValue.name} already exists`,
      });
    }

    console.error(e);
    res
      .status(constants.HTTP_STATUS_INTERNAL_SERVER_ERROR)
      .json({ success: false, error: "an internal server error occurred" });
  }
}

async function get(
  req: NextApiRequest,
  res: NextApiResponse<QueryResponse<Asset[]>>
) {
  try {
    let filter: AssetQuery = {};
    let filterActivated = false;
    let page: string | null | undefined = undefined;

    if (req.query.tags) {
      filter.tags =
        typeof req.query.tags === "string"
          ? req.query.tags.split(",")
          : req.query.tags;
      filterActivated = true;
    }

    if (req.query.page) {
      if (req.query.page === "null") page = null;
      else if (typeof req.query.page === "string") page = req.query.page;
    }

    if (req.query.libraries) {
      filter.libraries =
        typeof req.query.libraries === "string"
          ? req.query.libraries.split(",")
          : req.query.libraries;
      filterActivated = true;
    }

    if (
      req.query.search &&
      typeof req.query.search === "string" &&
      req.query.search.trim().length > 0
    ) {
      filter.query = req.query.search.trim();
      filterActivated = true;
    }

    let libraries = filterActivated
      ? await queryAsset(page, filter)
      : await getAllAssets(page);

    if (req.query.compressed) {
      libraries = await compressAssets(libraries);
    }

    res
      .status(constants.HTTP_STATUS_OK)
      .json({ success: true, data: libraries as any });
  } catch (e) {
    console.error(e);
    res
      .status(constants.HTTP_STATUS_INTERNAL_SERVER_ERROR)
      .json({ success: false, error: "an internal server error occurred" });
  }
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  switch (req.method) {
    case "GET":
      return get(req, res);
    case "POST":
      return create(req, res);
    default:
      res.status(constants.HTTP_STATUS_METHOD_NOT_ALLOWED).end();
  }
}

export const config = {
  api: {
    bodyParser: {
      sizeLimit: "100mb",
    },
  },
};
