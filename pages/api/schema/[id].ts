import { NextApiRequest, NextApiResponse } from "next";
import {
  ModificationResponse,
  QueryResponse,
} from "../../../@types/APIResponse";
import { Tag } from "../../../@types/Tag";
import { constants } from "http2";
import {
  addSchemaEntry,
  deleteKeyFromSchema,
  getAssetSchema,
  getLibrarySchema,
  getTagsSchema,
  purgeKeyAndDataFromSchema,
  updateSchemaEntry,
} from "../../../lib/dao/settings";
import { TableSchema } from "../../../components/FormattedTable";
import zod from "zod";

async function get(
  req: NextApiRequest,
  res: NextApiResponse<QueryResponse<TableSchema>>,
  id: "tag" | "asset" | "library"
) {
  try {
    let search: TableSchema = undefined as any;
    if (id === "tag") search = await getTagsSchema();
    if (id === "asset") search = await getAssetSchema();
    if (id === "library") search = await getLibrarySchema();

    res.status(constants.HTTP_STATUS_OK).json({
      success: true,
      data: search,
    });
  } catch (e: any) {
    console.error(e);
    res.status(constants.HTTP_STATUS_INTERNAL_SERVER_ERROR).json({
      success: false,
      error: "an internal server error occurred while querying",
    });
  }
}

const SchemaUpdateValidator = zod
  .object({
    ".purge": zod.string(),
  })
  .or(
    zod.object({
      ".delete": zod.string(),
    })
  )
  .or(
    zod.object({
      ".add": zod.object({
        display: zod.string(),
        access: zod.string(),
        format: zod.enum(["text", "image", "link", "code", "icon"]),
      }),
    })
  )
  .or(
    zod.object({
      original: zod.string(),
      property: zod.object({
        display: zod.string(),
        access: zod.string(),
        format: zod.enum(["text", "image", "link", "code", "icon"]),
      }),
    })
  );

async function patch(
  req: NextApiRequest,
  res: NextApiResponse<QueryResponse<TableSchema>>,
  id: "tag" | "asset" | "library"
) {
  const parse = SchemaUpdateValidator.safeParse(req.body);
  if (!parse.success) {
    const flattened = parse.error.flatten();
    const error =
      flattened.formErrors.join("; ") +
      Object.entries(flattened.fieldErrors)
        .map(([k, v]) => `${k}: ${v.join(", ")}`)
        .join("; ");
    return res.status(constants.HTTP_STATUS_BAD_REQUEST).send({
      success: false,
      error,
    });
  }

  let result: TableSchema | null = null;
  if (".purge" in parse.data && parse.data[".purge"]) {
    result = await purgeKeyAndDataFromSchema(parse.data[".purge"], id);
  } else if (".delete" in parse.data && parse.data[".delete"]) {
    result = await deleteKeyFromSchema(parse.data[".delete"], id);
  } else if (".add" in parse.data && parse.data[".add"]) {
    result = await addSchemaEntry(id, parse.data[".add"]);
  } else if ("original" in parse.data) {
    result = await updateSchemaEntry(
      parse.data.original,
      id,
      parse.data.property
    );
  }

  if (result === null) {
    res.status(constants.HTTP_STATUS_INTERNAL_SERVER_ERROR).json({
      success: false,
      error: "Failed to update the schema - are you sure the key is valid?",
    });
  } else {
    res.status(constants.HTTP_STATUS_OK).send({
      success: true,
      data: result,
    });
  }
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<QueryResponse<Tag> | ModificationResponse | void>
) {
  const { id } = req.query;

  if (typeof id !== "string" || !["tag", "asset", "library"].includes(id)) {
    return res.status(constants.HTTP_STATUS_NOT_FOUND).end();
  }

  const parsed = id as "tag" | "asset" | "library";

  switch (req.method) {
    case "GET":
      return get(req, res, parsed);
    case "PATCH":
      return patch(req, res, parsed);
    default:
      res.status(constants.HTTP_STATUS_METHOD_NOT_ALLOWED).end();
  }
}
