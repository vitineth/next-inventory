import { GetServerSideProps, NextPage } from "next";
import SharedHeader from "../../components/SharedHeader";
import { Container, Header, Icon } from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";
import { getLibrarySchema } from "../../lib/dao/settings";
import FormattedTable, { TableSchema } from "../../components/FormattedTable";
import Link from "next/link";
import { Library } from "../../@types/Library";
import { getAllLibraries } from "../../lib/dao/libraries";
import { SupplementalLibrarySchema } from "../../components/SupplementalSchemas";
import bindWithDefaultLayout from "../../components/DefaultLayout";
import requireAuth from "../../utils/authentication";
import { FormattedMessage } from "react-intl";

type LibraryPropType = {
  schema: TableSchema;
  libraries: Library[];
};

export const getServerSideProps: GetServerSideProps<LibraryPropType> = async (
  context
) => {
  const [schema, libraries] = await Promise.all([
    getLibrarySchema(),
    getAllLibraries(),
  ]);
  return {
    props: {
      schema,
      libraries,
    },
  };
};

const Index: NextPage<LibraryPropType> = ({ schema, libraries }) => {
  const clone = [...schema.columns, ...SupplementalLibrarySchema];

  return (
    <>
      <Header size="huge">
        <FormattedMessage defaultMessage={"Libraries"} />
      </Header>
      <FormattedTable configuration={{ columns: clone }} data={libraries} />
    </>
  );
};

export default requireAuth(
  bindWithDefaultLayout(Index, "Library < Next Inventory Management")
);
