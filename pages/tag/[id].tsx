import "semantic-ui-css/semantic.min.css";
import React, { useEffect, useState } from "react";
import { Asset } from "../../@types/Asset";
import { GetServerSideProps } from "next";
import { getAsset } from "../../lib/dao/assets";
import SharedHeader from "../../components/SharedHeader";
import FormattedTable, { TableSchema } from "../../components/FormattedTable";
import {
  defaultAssetSchema,
  defaultLibrarySchema,
  getAssetSchema,
  getTagsSchema,
} from "../../lib/dao/settings";
import {
  Button,
  Confirm,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Label,
  Message,
} from "semantic-ui-react";
import RenderedProperty from "../../components/RenderedProperty";
import Link from "next/link";
import { QueryResponse } from "../../@types/APIResponse";
import TagSelector, { LibrarySelector } from "../../components/TagSelector";
import EditableProperty from "../../components/EditableProperty";
import { Tag } from "../../@types/Tag";
import { getTag } from "../../lib/dao/tags";
import PromiseLoader from "../../components/Loader";
import {
  SupplementalAssetPostSchema,
  SupplementalAssetPreSchema,
} from "../../components/SupplementalSchemas";
import bindWithDefaultLayout from "../../components/DefaultLayout";
import Head from "next/head";
import { useRouter } from "next/router";
import requireAuth from "../../utils/authentication";
import { FormattedMessage, useIntl } from "react-intl";

type TagIDProps = {
  tag: Tag;
  schema: TableSchema;
  assetSchema: TableSchema;
};

export const getServerSideProps: GetServerSideProps<TagIDProps> = async (
  context
) => {
  const [tag, schema, assetSchema] = await Promise.allSettled([
    await getTag(context.query.id as string),
    getTagsSchema(),
    getAssetSchema(),
  ]);

  if (tag.status === "rejected" || tag.value === null)
    return { notFound: true };

  const parsed =
    schema.status === "rejected"
      ? { columns: defaultAssetSchema }
      : schema.value;
  const parsedAsset =
    assetSchema.status === "rejected"
      ? { columns: defaultLibrarySchema }
      : assetSchema.value;

  return {
    props: {
      tag: tag.value,
      schema: parsed,
      assetSchema: parsedAsset,
    },
  };
};

const Id: React.FunctionComponent<TagIDProps> = ({
  tag,
  schema,
  assetSchema,
}) => {
  const [state, setState] = useState(tag);
  const [error, setError] = useState<{ title: string; body: string } | null>(
    null
  );
  const [isLoading, setLoading] = useState(false);
  const [assets, setAssets] = useState<Asset[] | null>(null);
  const [showDeleteConfirm, setShowDeleteConfirm] = useState(false);
  const router = useRouter();
  const intl = useIntl();

  useEffect(() => {
    if (!isLoading && assets === null) {
      setLoading(true);
      fetch(`/api/asset?tags=${tag.name}`)
        .then((d) => d.json())
        .then((d: QueryResponse<Asset[]>) => {
          if (d.success) setAssets(d.data);
          else
            setError({
              title: intl.formatMessage({
                defaultMessage: "Failed to load assets",
              }),
              body: d.error,
            });
        })
        .then((d) => setLoading(false))
        .catch((e: Error) => {
          setError({
            title: intl.formatMessage({
              defaultMessage: "Failed to load assets",
            }),
            body: e.message,
          });
          setLoading(false);
        });
    }
  }, [isLoading, assets, tag.name, intl]);

  const patchProperty = (property: string, value: string | undefined) => {
    let modification: Record<string, any> = {};

    if (value === undefined) {
      modification[".unset"] = [property];
    } else {
      modification[property] = value;
    }

    fetch(`/api/tag/${state.name}`, {
      method: "PATCH",
      body: JSON.stringify(modification),
      headers: { "Content-Type": "application/json" },
    })
      .then((d) => d.json())
      .then((r: QueryResponse<any>) => {
        if (r.success) {
          setState((s) => ({
            ...s,
            [property]: value,
          }));
          setError(null);
        } else {
          setError({
            title: intl.formatMessage(
              {
                defaultMessage: "Failed to assign {property}",
              },
              { property }
            ),
            body: r.error,
          });
        }
      });
  };

  return (
    <>
      <Head>
        <title>
          {state.name} &lt; <FormattedMessage defaultMessage={"Tag"} /> &lt;{" "}
          <FormattedMessage defaultMessage={"Next Inventory Management"} />
        </title>
      </Head>
      {error ? (
        <Message error header={error.title} content={error.body} />
      ) : undefined}
      <Confirm
        open={showDeleteConfirm}
        onCancel={() => setShowDeleteConfirm(false)}
        onConfirm={() => {
          fetch(`/api/tag/${state.name}`, {
            method: "DELETE",
          })
            .then((r) => {
              if (r.ok) return router.push("/tag");
              throw new Error(
                intl.formatMessage({ defaultMessage: "Failed to delete" })
              );
            })
            .catch(() => {
              alert(
                intl.formatMessage({
                  defaultMessage: "Failed to delete pipeline",
                })
              );
            });
        }}
      />

      <Header size="huge">
        <EditableProperty
          schema={{ access: "name", display: "Name", format: "text" }}
          onChange={(v) => {
            if (v === undefined) return;
            patchProperty("name", v);
          }}
          value={state.name}
        />
        <Button.Group floated="right">
          <Button color="red" onClick={() => setShowDeleteConfirm(true)}>
            <FormattedMessage defaultMessage={"Delete"} />
          </Button>
        </Button.Group>
      </Header>
      {schema.columns.map((column) => (
        <EditableProperty
          onChange={(v) => patchProperty(column.access, v)}
          schema={column}
          value={state[column.access]}
          includeHeader={true}
          key={column.access}
        />
      ))}

      <Divider />
      <Header size="large">
        <FormattedMessage defaultMessage={"Linked Assets"} /> (query &quot;
        <code style={{ fontSize: "0.8em" }}>tag:{tag.name}</code>&quot;)
      </Header>
      <PromiseLoader condition={!isLoading && assets !== null}>
        {() => (
          <FormattedTable
            configuration={{
              columns: [
                ...SupplementalAssetPreSchema,
                ...assetSchema.columns,
                ...SupplementalAssetPostSchema,
              ],
            }}
            data={assets ?? []}
          />
        )}
      </PromiseLoader>
    </>
  );
};

export default requireAuth(bindWithDefaultLayout(Id));
