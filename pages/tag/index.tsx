import { GetServerSideProps, NextPage } from "next";
import SharedHeader from "../../components/SharedHeader";
import { Container, Header, Icon } from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";
import { getTagsSchema } from "../../lib/dao/settings";
import FormattedTable, { TableSchema } from "../../components/FormattedTable";
import { getAllTags } from "../../lib/dao/tags";
import { ShallowTag, Tag } from "../../@types/Tag";
import Link from "next/link";
import { SupplementalTagSchema } from "../../components/SupplementalSchemas";
import bindWithDefaultLayout from "../../components/DefaultLayout";
import requireAuth from "../../utils/authentication";
import { FormattedMessage } from "react-intl";

type TagPropType = {
  schema: TableSchema;
  tags: Tag[];
};

export const getServerSideProps: GetServerSideProps<TagPropType> = async (
  context
) => {
  const [schema, tags] = await Promise.all([getTagsSchema(), getAllTags()]);
  return {
    props: {
      schema,
      tags,
    },
  };
};

const Index: NextPage<TagPropType> = ({ schema, tags }) => {
  const clone: TableSchema["columns"] = [
    ...SupplementalTagSchema,
    ...schema.columns,
  ];

  return (
    <>
      <Header size="huge">
        <FormattedMessage defaultMessage={"Tags"} />
      </Header>
      <FormattedTable configuration={{ columns: clone }} data={tags} />
    </>
  );
};

export default requireAuth(
  bindWithDefaultLayout(Index, "Tag < Next Inventory Management")
);
