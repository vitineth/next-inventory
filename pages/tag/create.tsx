import "semantic-ui-css/semantic.min.css";
import { NextPage } from "next";
import { useEffect, useState } from "react";
import SharedHeader from "../../components/SharedHeader";
import { Button, Container, Form, Loader, Message } from "semantic-ui-react";
import { TagCreate } from "../../@types/Tag";
import { TableSchema } from "../../components/FormattedTable";
import IconDropdown from "../../components/IconDropdown";
import { ModificationResponse } from "../../@types/APIResponse";
import SchemaEditElement from "../../components/SchemaEditElement";
import bindWithDefaultLayout from "../../components/DefaultLayout";
import requireAuth from "../../utils/authentication";
import { FormattedMessage, useIntl } from "react-intl";

const Create: NextPage = () => {
  const [isLoading, setLoading] = useState(false);
  const [schema, setSchema] = useState<TableSchema | null>(null);
  const [error, setError] = useState<{ title: string; body: string } | null>(
    null
  );
  const intl = useIntl();
  const [creation, setCreation] = useState<Partial<TagCreate>>({});
  const [cachedIconHandler, setCachedIconHandler] = useState<
    Record<string, (v: string) => void>
  >({});

  useEffect(() => {
    if (!isLoading && schema === null) {
      setLoading(true);
      fetch("/api/schema/asset")
        .then((d) => d.json())
        .then((d) => setSchema(d.data ?? { columns: [] }))
        .then(() => setLoading(false));
    }
  }, [isLoading, schema]);

  if (isLoading) {
    return <Loader />;
  }

  let components = (schema?.columns ?? [])
    .filter((e) => typeof e.format === "string")
    .map((e) => {
      if (e.format === "icon") {
        if (cachedIconHandler[e.access] === undefined)
          setCachedIconHandler((p) => ({
            ...p,
            [e.access]: (v: string) =>
              setCreation((p) => ({ ...p, [e.access]: v })),
          }));
        return (
          <SchemaEditElement
            property={e}
            onSet={cachedIconHandler[e.access]}
            value={creation[e.access]}
          />
        );
      } else {
        return (
          <SchemaEditElement
            property={e}
            onSet={(v) => setCreation((p) => ({ ...p, [e.access]: v }))}
            value={creation[e.access]}
          />
        );
      }
    });

  const submit = () => {
    if (creation.name === undefined || creation.name.trim() === "") {
      setError({
        title: intl.formatMessage({ defaultMessage: "Invalid submission" }),
        body: intl.formatMessage({
          defaultMessage: 'A value for "Name" is required',
        }),
      });
      return;
    }
    if (creation.icon === undefined || creation.icon.trim() === "") {
      setError({
        title: intl.formatMessage({ defaultMessage: "Invalid submission" }),
        body: intl.formatMessage({
          defaultMessage: 'A value for "Icon" is required',
        }),
      });
      return;
    }

    fetch("/api/tag", {
      method: "POST",
      body: JSON.stringify(creation),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((r) => r.json())
      .then((d: ModificationResponse) => {
        if (d.success) {
          if (d.id) window.location.assign(`/tag/${d.id}`);
          else window.location.assign("/tag");
        } else {
          setError({
            title: intl.formatMessage({ defaultMessage: "Invalid submission" }),
            body: d.error,
          });
        }
      })
      .catch((e: Error) => {
        setError({
          title: intl.formatMessage({ defaultMessage: "Invalid submission" }),
          body: e.message,
        });
      });
  };

  return (
    <>
      {error !== null ? (
        <Message error={true} header={error.title} content={error.body} />
      ) : undefined}
      <Form>
        {JSON.stringify(creation)}

        <Form.Input
          label={intl.formatMessage({ defaultMessage: "Name" })}
          type="text"
          required
          value={creation.name}
          onChange={(_, d) => setCreation((p) => ({ ...p, name: d.value }))}
        />

        <Form.Field required>
          <label>
            <FormattedMessage defaultMessage={"Icon"} />
          </label>
          <IconDropdown
            value={creation.icon}
            onIconSelected={(v) => setCreation((p) => ({ ...p, icon: v }))}
          />
        </Form.Field>

        {components}

        <Button onClick={submit} primary>
          <FormattedMessage defaultMessage={"Create"} />
        </Button>
      </Form>
    </>
  );
};

export default requireAuth(
  bindWithDefaultLayout(Create, "Create < Tag < Next Inventory Management")
);
