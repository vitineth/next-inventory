import "semantic-ui-css/semantic.min.css";
import { GetServerSideProps, NextPage } from "next";
import SharedHeader from "../components/SharedHeader";
import Head from "next/head";
import {
  Button,
  Container,
  Divider,
  Form,
  Header,
  Label,
  Message,
  Modal,
  Popup,
  Table,
} from "semantic-ui-react";
import { Format, TableSchema } from "../components/FormattedTable";
import {
  defaultAssetSchema,
  defaultLibrarySchema,
  defaultTagSchema,
  getAssetSchema,
  getLibrarySchema,
  getTagsSchema,
} from "../lib/dao/settings";
import React, { useState } from "react";
import { ModificationResponse, QueryResponse } from "../@types/APIResponse";
import {
  SupplementalAssetPostSchema,
  SupplementalAssetPreSchema,
  SupplementalLibrarySchema,
  SupplementalTagSchema,
} from "../components/SupplementalSchemas";
import bindWithDefaultLayout from "../components/DefaultLayout";
import requireAuth from "../utils/authentication";
import { FormattedMessage, useIntl } from "react-intl";

type SchemaProps = {
  asset: TableSchema;
  tag: TableSchema;
  library: TableSchema;
};

export const getServerSideProps: GetServerSideProps<SchemaProps> = async (
  context
) => {
  const [assetPromise, tagPromise, libraryPromise] = await Promise.allSettled([
    getAssetSchema(),
    getTagsSchema(),
    getLibrarySchema(),
  ]);

  let asset: TableSchema = { columns: defaultAssetSchema };
  let tag: TableSchema = { columns: defaultTagSchema };
  let library: TableSchema = { columns: defaultLibrarySchema };

  if (assetPromise.status !== "rejected") asset = assetPromise.value;
  if (tagPromise.status !== "rejected") tag = tagPromise.value;
  if (libraryPromise.status !== "rejected") library = libraryPromise.value;

  return {
    props: {
      tag,
      asset,
      library,
    },
  };
};

export const SchemaTable: React.FunctionComponent<{
  schema: TableSchema;
  readOnly: TableSchema["columns"];
  target?: string;
  setError: (title: string, body: string) => void;
  onSuccess: (schema: TableSchema["columns"]) => void;
}> = ({ schema, readOnly, target, onSuccess, setError }) => {
  const intl = useIntl();
  const remove = (method: "purge" | "delete", id: string) => {
    if (!target) {
      const copy = [...schema.columns].filter((e) => e.access !== id);
      onSuccess(copy);
      return;
    }

    fetch(target, {
      method: "PATCH",
      body: JSON.stringify({
        [`.${method}`]: id,
      }),
      headers: { "Content-Type": "application/json" },
    })
      .then((d) => d.json())
      .then((d: QueryResponse<TableSchema>) => {
        if (d.success) onSuccess(d.data.columns);
        else
          setError(
            intl.formatMessage({ defaultMessage: "Failed to update property" }),
            d.error
          );
      })
      .catch((e: Error) => {
        setError(
          intl.formatMessage({ defaultMessage: "Failed to update property" }),
          e.message
        );
      });
  };

  const update = (
    originalID: string | undefined,
    property: TableSchema["columns"][number]
  ) => {
    if (!target) {
      const copy = [...schema.columns];
      if (originalID)
        onSuccess(
          copy.filter((e) => e.access !== originalID).concat([property])
        );
      else onSuccess(copy.concat([property]));
      return;
    }

    const body = originalID
      ? { original: originalID, property }
      : { ".add": property };

    fetch(target, {
      method: "PATCH",
      body: JSON.stringify(body),
      headers: { "Content-Type": "application/json" },
    })
      .then((d) => d.json())
      .then((d: QueryResponse<TableSchema>) => {
        if (d.success) onSuccess(d.data.columns);
        else
          setError(
            intl.formatMessage({ defaultMessage: "Failed to update property" }),
            d.error
          );
      })
      .catch((e: Error) => {
        setError(
          intl.formatMessage({ defaultMessage: "Failed to update property" }),
          e.message
        );
      });
  };

  const [activeEdit, setActiveEdit] = useState<{
    property: TableSchema["columns"][number];
    original?: TableSchema["columns"][number];
  } | null>(null);
  const [modalOpen, setModalOpen] = useState(false);

  return (
    <>
      <Modal
        onOpen={() => setModalOpen(true)}
        onClose={() => setModalOpen(false)}
        open={modalOpen}
        // trigger={<Button>test</Button>}⩦
      >
        {activeEdit ? (
          <>
            {activeEdit.original ? (
              <Modal.Header>
                <FormattedMessage
                  defaultMessage={"Edit {display} ({access})"}
                  values={{
                    display: activeEdit.original.display,
                    access: activeEdit.original.access,
                  }}
                />
              </Modal.Header>
            ) : (
              <Modal.Header>
                <FormattedMessage defaultMessage={"Create new property"} />
              </Modal.Header>
            )}
            <Modal.Content>
              <Form>
                <Form.Input
                  label={intl.formatMessage({
                    defaultMessage: "Data Identifier",
                  })}
                  type="text"
                  fluid
                  value={activeEdit.property.access}
                  onChange={(_, d) =>
                    setActiveEdit({
                      ...activeEdit,
                      property: {
                        ...activeEdit.property,
                        access: d.value,
                      },
                    })
                  }
                />
                <Form.Input
                  label={intl.formatMessage({ defaultMessage: "Display Name" })}
                  type="text"
                  fluid
                  value={activeEdit.property.display}
                  onChange={(_, d) =>
                    setActiveEdit({
                      ...activeEdit,
                      property: {
                        ...activeEdit.property,
                        display: d.value,
                      },
                    })
                  }
                />
                <Form.Select
                  label={intl.formatMessage({ defaultMessage: "Format" })}
                  fluid
                  value={activeEdit.property.format as string}
                  onChange={(_, d) =>
                    setActiveEdit({
                      ...activeEdit,
                      property: {
                        ...activeEdit.property,
                        format: d.value as any,
                      },
                    })
                  }
                  options={[
                    {
                      text: intl.formatMessage({ defaultMessage: "Text" }),
                      value: "text",
                    },
                    {
                      text: intl.formatMessage({
                        defaultMessage: "Image (file upload)",
                      }),
                      value: "image",
                    },
                    {
                      text: intl.formatMessage({
                        defaultMessage:
                          "Link (will be converted to clickable link)",
                      }),
                      value: "link",
                    },
                    {
                      text: intl.formatMessage({
                        defaultMessage: "Code (rendered as monospace font)",
                      }),
                      value: "code",
                    },
                    {
                      text: intl.formatMessage({
                        defaultMessage: "Icon (with chooser)",
                      }),
                      value: "icon",
                    },
                  ]}
                />
              </Form>
            </Modal.Content>
            <Modal.Actions>
              <Button
                onClick={() => {
                  setModalOpen(false);
                  setActiveEdit(null);
                }}
              >
                <FormattedMessage defaultMessage={"Cancel"} />
              </Button>
              <Button
                primary
                onClick={() => {
                  if (activeEdit.property.access.trim().length === 0) return;
                  if (activeEdit.property.display.trim().length === 0) return;

                  update(activeEdit.original?.access, activeEdit.property);
                  setModalOpen(false);
                  setActiveEdit(null);
                }}
              >
                <FormattedMessage defaultMessage={"Save"} />
              </Button>
            </Modal.Actions>
          </>
        ) : undefined}
      </Modal>
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>
              <FormattedMessage defaultMessage={"ID"} />
            </Table.HeaderCell>
            <Table.HeaderCell>
              <FormattedMessage defaultMessage={"Name"} />
            </Table.HeaderCell>
            <Table.HeaderCell>
              <FormattedMessage defaultMessage={"Type"} />
            </Table.HeaderCell>
            <Table.HeaderCell>
              <FormattedMessage defaultMessage={"Edit"} />
            </Table.HeaderCell>
            <Table.HeaderCell>
              <FormattedMessage defaultMessage={"Remove"} />
            </Table.HeaderCell>
            <Table.HeaderCell>
              <FormattedMessage defaultMessage={"Purge"} />
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {readOnly.map((e) => (
            <Table.Row key={e.access}>
              <Table.Cell>{e.access}</Table.Cell>
              <Table.Cell>{e.display}</Table.Cell>
              <Table.Cell>
                {typeof e.format === "function" ? (
                  <Label>
                    <FormattedMessage defaultMessage={"Custom renderer"} />
                  </Label>
                ) : (
                  e.format
                )}
              </Table.Cell>
              <Table.Cell
                colSpan={3}
                style={{
                  textAlign: "center",
                  fontStyle: "italic",
                  color: "gainsboro",
                }}
              >
                <FormattedMessage defaultMessage={"read only"} />
              </Table.Cell>
            </Table.Row>
          ))}
          {schema.columns.map((e) => (
            <Table.Row key={e.access}>
              <Table.Cell>{e.access}</Table.Cell>
              <Table.Cell>{e.display}</Table.Cell>
              <Table.Cell>{String(e.format)}</Table.Cell>
              <Table.Cell>
                <Button
                  icon="edit"
                  circular
                  size="tiny"
                  onClick={() => {
                    setActiveEdit({
                      property: { ...e },
                      original: { ...e },
                    });
                    setModalOpen(true);
                  }}
                />
              </Table.Cell>
              <Table.Cell>
                <Popup
                  position="right center"
                  inverted
                  content={intl.formatMessage({
                    defaultMessage:
                      "Warning: this will remove the property but all content will remain on the server. If you re-add the property at a later date, the data will be visible again",
                  })}
                  trigger={
                    <Button
                      icon="trash"
                      onClick={() => remove("delete", e.access)}
                      circular
                      size="tiny"
                    />
                  }
                />
              </Table.Cell>
              <Table.Cell>
                <Popup
                  position="right center"
                  inverted
                  content={intl.formatMessage({
                    defaultMessage:
                      "Warning: this will delete all traces of this property from storage",
                  })}
                  trigger={
                    <Button
                      icon="trash"
                      onClick={() => remove("purge", e.access)}
                      circular
                      size="tiny"
                    />
                  }
                />
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
      <Button
        icon="edit"
        content="Add new"
        onClick={() => {
          setActiveEdit({
            property: { access: "", format: "text", display: "" },
            original: undefined,
          });
          setModalOpen(true);
        }}
      />
    </>
  );
};

const Schema: NextPage<SchemaProps> = ({ asset, tag, library }) => {
  const [assetSchema, setAssetSchema] = useState(asset);
  const [tagSchema, setTagSchema] = useState(tag);
  const [librarySchema, setLibrarySchema] = useState(library);

  const [error, setError] = useState<{ title: string; body: string } | null>(
    null
  );

  return (
    <>
      <Head>
        <title>
          <FormattedMessage defaultMessage={"Schema Management :: NIM"} />
        </title>
      </Head>
      <Header size="large">
        <FormattedMessage defaultMessage={"Schema Configuration"} />
      </Header>
      {error ? (
        <Message error header={error.title} content={error.body} />
      ) : undefined}

      <Header size="medium">
        <FormattedMessage defaultMessage={"Asset"} />
      </Header>
      <SchemaTable
        schema={assetSchema}
        readOnly={[
          ...SupplementalAssetPreSchema,
          ...SupplementalAssetPostSchema,
        ]}
        target={"/api/schema/asset"}
        setError={(t, b) => setError({ title: t, body: b })}
        onSuccess={(p) => setAssetSchema({ columns: p })}
      />
      <Divider />

      <Header size="medium">
        <FormattedMessage defaultMessage={"Tag"} />
      </Header>
      <SchemaTable
        schema={tagSchema}
        readOnly={SupplementalTagSchema}
        target={"/api/schema/tag"}
        setError={(t, b) => setError({ title: t, body: b })}
        onSuccess={(p) => setTagSchema({ columns: p })}
      />
      <Divider />

      <Header size="medium">
        <FormattedMessage defaultMessage={"Library"} />
      </Header>

      <SchemaTable
        schema={librarySchema}
        readOnly={SupplementalLibrarySchema}
        target={"/api/schema/library"}
        setError={(t, b) => setError({ title: t, body: b })}
        onSuccess={(p) => setLibrarySchema({ columns: p })}
      />
      <Divider />
    </>
  );
};

export default requireAuth(bindWithDefaultLayout(Schema));
