const data = await fs.readJson('content/locales/en.json');
const results = await Promise.allSettled(Object.entries(data).map(async ([key, value]) => [key, String(await $`trans -b en:de ${value}`).trim()]));
const successes = results.filter((e) => e.status === 'fulfilled').map((e) => e.value);
const failures = results.filter((e) => e.status === 'rejected').map((e) => e.value[0]);

console.log(`${successes.length} succeeded, ${failures.length} failed (${failures})`);
await fs.writeJson('content/locales/de.json', Object.fromEntries(successes), {encoding: 'utf8'});