import React, { useMemo, useState } from "react";
import { Dropdown, Icon } from "semantic-ui-react";
import { ICONS } from "../utils/icons";
import { injectIntl, IntlShape } from "react-intl";

export type IconDropdownProps = {
  value?: string;
  onIconSelected?: (icon: string) => void;
  intl: IntlShape;
};

// eslint-disable-next-line react/display-name
const IconDropdown = React.memo(
  injectIntl(function IconDropdown(props: IconDropdownProps) {
    const [value, setValue] = useState<string | undefined>(props.value);

    const iconOptions = useMemo(
      () =>
        ICONS.map((e) => ({
          key: e,
          text: e,
          value: e,
          label: <Icon name={e as any} />,
        })),
      []
    );

    return (
      <Dropdown
        button
        className="icon"
        floating
        fluid
        style={{ marginBottom: "10px" }}
        labeled
        icon={value ?? "circle outline"}
        options={iconOptions}
        onChange={(_, v) => {
          if (props.onIconSelected) props.onIconSelected(v.value as string);
          setValue(v.value as string);
        }}
        value={value}
        search
        selection
        placeholder={props.intl.formatMessage({ defaultMessage: "Icon" })}
      />
    );
  }),
  (p, n) => {
    return p.value === n.value;
  }
);

export default IconDropdown;
