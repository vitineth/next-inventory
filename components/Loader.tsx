import React, { useState } from "react";
import { Loader, Message } from "semantic-ui-react";
import { FormattedMessage } from "react-intl";

type PromiseLoaderProps<T> =
  | {
      promise: Promise<T>;
      children: (v: T) => React.ReactNode;
    }
  | {
      condition: boolean;
      children: () => React.ReactNode;
    };

const PromiseLoader: React.FunctionComponent<PromiseLoaderProps<any>> = <T,>(
  props: PromiseLoaderProps<T>
) => {
  const [content, setContent] = useState<T | Error | null>(null);

  if (content === null) {
    if ("promise" in props) {
      props.promise.then((v) => setContent(v)).catch((e) => setContent(e));
    } else {
      if (props.condition) setContent(true as any);
    }
  }

  if (content === null) {
    return <Loader active />;
  }

  if (content instanceof Error) {
    return (
      <Message negative>
        <Message.Header>
          <FormattedMessage defaultMessage="Promise failed to load" />
        </Message.Header>
        <p>
          <FormattedMessage defaultMessage="The promise loading this data was not loaded resolved, it failed with the error" />
          {content.message}
        </p>
      </Message>
    );
  }

  return <>{props.children(content)}</>;
};

export default PromiseLoader;
