import { useEffect, useState } from "react";
import { Tag } from "../@types/Tag";
import { Dropdown, Form, Loader, Message } from "semantic-ui-react";
import { FormattedMessage, injectIntl, IntlShape } from "react-intl";

export const TagCreator = injectIntl(
  (props: { selected: string[]; intl: IntlShape }) => {
    const [isLoading, setLoading] = useState(false);
    const [tags, setTags] = useState<Tag[] | null>(null);

    useEffect(() => {
      if (!isLoading && tags === null) {
        setLoading(true);
        fetch("/api/tag")
          .then((d) => d.json())
          .then((d) => setTags(d.data))
          .then(() => setLoading(false));
      }
    }, [isLoading, tags]);

    if (isLoading) {
      return <Loader />;
    }

    if (tags === null) {
      return (
        <Message
          error
          header={props.intl.formatMessage({
            defaultMessage: "Failed to load tags",
          })}
          content={props.intl.formatMessage({
            defaultMessage: "There was an error when fetching the tags! ",
          })}
        />
      );
    }

    return (
      <Form.Field>
        <label>
          <FormattedMessage defaultMessage="Tags" />
        </label>
        <Dropdown
          placeholder={props.intl.formatMessage({ defaultMessage: "Tags" })}
          fluid
          multiple
          search
          selection
          options={(tags ?? []).map((v) => ({
            text: v.name,
            value: v.name,
          }))}
          value={tags
            .filter((e) => props.selected.includes(e.name))
            .map((e) => e.name)}
          // onChange={ (_, i) => setCreation((p) => ({ ...p, tags: i.value as string[] })) }
        />
      </Form.Field>
    );
  }
);
