import "semantic-ui-css/semantic.min.css";
import { ReactElement } from "react";
import { NextPage } from "next";
import { PageWithLayout } from "../pages/_app";
import SharedHeader from "./SharedHeader";
import { Container } from "semantic-ui-react";
import Head from "next/head";

// TODO: internationalise
export default function bindWithDefaultLayout<T>(
  element: NextPage<T>,
  title: string = "Next Inventory Manager"
): PageWithLayout<T> {
  const copy: PageWithLayout<T> = element;
  copy.getLayout = (page: ReactElement) => (
    <>
      <Head>
        <title>{title}</title>
      </Head>
      <SharedHeader />

      <Container>{page}</Container>
    </>
  );

  return copy;
}
