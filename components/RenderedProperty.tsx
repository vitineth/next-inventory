import { TableSchema } from "./FormattedTable";
import { Header, HeaderProps, Icon, Image } from "semantic-ui-react";
import Link from "next/link";
import React from "react";
import { FormattedMessage } from "react-intl";

type RenderedPropertyType = {
  schema: TableSchema["columns"][number];
  value?: string;
  blankIfUndefined?: boolean;
  includeHeader?: boolean;
  headerSize?: HeaderProps["size"];
};

const RenderedProperty: React.FunctionComponent<RenderedPropertyType> = (
  props
) => {
  const fail = () => {
    if (props.blankIfUndefined) return null;

    const span = (
      <span style={{ fontStyle: "italic", color: "gainsboro" }}>
        <FormattedMessage defaultMessage={"unset"} />
      </span>
    );
    if (props.includeHeader)
      return (
        <>
          <Header size={props.headerSize ?? "large"}>
            {props.schema.display}
          </Header>
          {span}
        </>
      );
    return span;
  };

  if (props.value === undefined) {
    return fail();
  }

  if (typeof props.schema.format === "function") {
    const formatted = props.schema.format(props.value);
    if (formatted === undefined || formatted === null) {
      return fail();
    }
    return <>{formatted}</>;
  }

  let element: React.ReactElement;
  switch (props.schema.format) {
    case "image":
      element = <Image alt={""} src={props.value} loading="lazy" />;
      break;
    case "link":
      element = (
        <Link href={props.value}>
          <a>
            <Icon name="chain" />{" "}
            <FormattedMessage defaultMessage={"External"} />
          </a>
        </Link>
      );
      break;
    case "code":
      element = <span style={{ fontFamily: "monospace" }}>{props.value}</span>;
      break;
    case "text":
      element = <span>{props.value}</span>;
      break;
    case "icon":
      element = <Icon name={props.value as any} />;
      break;
    default:
      element = (
        <span>
          <FormattedMessage defaultMessage={"Unknown format"} />{" "}
          {props.schema.format}
        </span>
      );
      break;
  }

  if (props.includeHeader) {
    return (
      <>
        <Header size={props.headerSize ?? "large"}>
          {props.schema.display}
        </Header>
        {element}
      </>
    );
  }

  return element;
};

export default RenderedProperty;
