import React from "react";
import { signIn, useSession } from "next-auth/react";
import { Message } from "semantic-ui-react";
import { injectIntl, IntlShape } from "react-intl";

const RequireAuthenticated = injectIntl(
  ({ children, intl }: { children: React.ReactNode; intl: IntlShape }) => {
    const { data, status } = useSession();

    if (status === "loading") {
      return null;
    }

    if (status === "unauthenticated") {
      signIn("keycloak");
      return (
        <Message
          header={intl.formatMessage({ defaultMessage: "Unauthenticated" })}
          content={intl.formatMessage({
            defaultMessage: "This route requires you to be logged in",
          })}
          error
        />
      );
    }

    return <>{children}</>;
  }
);

export default RequireAuthenticated;
