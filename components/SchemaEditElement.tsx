import { Format, TableSchema } from "./FormattedTable";
import { Form, Input } from "semantic-ui-react";
import IconDropdown from "./IconDropdown";
import { FormattedMessage } from "react-intl";

type SchemaEditElementProps = {
  property: TableSchema["columns"][number];
  value?: string;
  onSet: (v: string) => void;
};

export default function SchemaEditElement(props: SchemaEditElementProps) {
  switch (props.property.format) {
    case "text":
      return (
        <Form.Input
          label={props.property.display}
          type="text"
          key={props.property.access}
          value={props.value ?? ""}
          onChange={(_, d) => props.onSet(d.value)}
        />
      );
    case "code":
      return (
        <Form.Field key={props.property.access}>
          <label>{props.property.display}</label>
          <Form.Input
            value={props.value ?? ""}
            type="text"
            className="code force-input"
            onChange={(_, d) => props.onSet(d.value)}
          />
        </Form.Field>
      );
    case "link":
      return (
        <Form.Field key={props.property.access}>
          <label>{props.property.display}</label>
          <Input
            value={props.value ?? ""}
            type="text"
            icon="chain"
            iconPosition="left"
            onChange={(_, d) => props.onSet(d.value)}
          />
        </Form.Field>
      );
    case "image":
      return (
        <Form.Input
          label={props.property.display}
          type="file"
          key={props.property.access}
          accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*"
          onChange={(c) => {
            const files = c.target.files;
            if (files && files.length > 0) {
              const reader = new FileReader();
              reader.readAsDataURL(files[0]);
              reader.onloadend = () => {
                if (typeof reader.result === "string")
                  props.onSet(reader.result);
                else if (
                  typeof reader.result === "object" &&
                  reader.result !== null
                )
                  props.onSet(
                    btoa(
                      String.fromCharCode.apply(
                        null,
                        new Uint8Array(reader.result) as any
                      )
                    )
                  );
              };
            }
          }}
        />
      );
    case "icon":
      return (
        <Form.Field>
          <label>{props.property.display}</label>
          <IconDropdown value={props.value} onIconSelected={props.onSet} />
        </Form.Field>
      );
    default:
      return (
        <span>
          <FormattedMessage defaultMessage={"Unknown format"} />
        </span>
      );
  }
}
