import Link from "next/link";
import { Icon, Label } from "semantic-ui-react";
import { TableSchema } from "./FormattedTable";
import { ShallowTag } from "../@types/Tag";
import { ShallowLibrary } from "../@types/Library";

export const SupplementalTagSchema: TableSchema["columns"] = [
  {
    access: "icon",
    format: "icon",
    display: "",
  },
  {
    access: "name",
    format: (n: string) => (
      <span>
        <Link href={`/tag/${n}`}>
          <a>
            <Icon name="chain" />
            {n}
          </a>
        </Link>
      </span>
    ),
    display: "Name",
  },
];

export const SupplementalLibrarySchema: TableSchema["columns"] = [
  {
    access: "name",
    format: (n: string) => (
      <span>
        <Link href={`/library/${n}`}>
          <a>
            <Icon name="chain" />
            {n}
          </a>
        </Link>
      </span>
    ),
    display: "Name",
  },
];

export const SupplementalAssetPreSchema: TableSchema["columns"] = [
  {
    display: "ID",
    format: (f: string) => (
      <Link href={`/asset/${f}`}>
        <a>
          <Icon name="chain" />
          {f}
        </a>
      </Link>
    ),
    access: "identifier",
  },
];

export const SupplementalAssetPostSchema: TableSchema["columns"] = [
  {
    display: "Tags",
    format: (f: ShallowTag[]) =>
      f.map((tag) => (
        <Label as="a" href={`/tag/${tag.name}`} key={`${tag.id}.tag`}>
          {tag.name}
        </Label>
      )),
    access: "tags",
  },
  {
    display: "Libraries",
    format: (f: ShallowLibrary[]) =>
      f.map((tag) => (
        <Label as="a" href={`/library/${tag.name}`} key={`${tag.id}.library`}>
          {tag.name}
        </Label>
      )),
    access: "libraries",
  },
];
