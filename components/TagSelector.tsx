import React, { useEffect, useMemo, useRef, useState } from "react";
import { Tag, TagCreate } from "../@types/Tag";
import { Dropdown, Loader } from "semantic-ui-react";
import { Library, LibraryCreate } from "../@types/Library";
import { injectIntl, IntlShape } from "react-intl";

type GenericSelectorProps<T> = {
  tags?: T[];
  active?: string[];
  onAdded?: (tag: string) => void;
  onRemoved?: (tag: string) => void;
  exclude?: string[];
  loadRoute: string;
  name: string;
  processor: (v: T) => { text: string; value: string };
  supportAddition?: (key: string) => Omit<T, "id">;
  intl: IntlShape;
};

type TagSelectorProps = Omit<
  GenericSelectorProps<Tag>,
  "loadRoute" | "name" | "processor" | "supportAddition"
> & { supportAddition?: boolean };
type LibrarySelectorProps = Omit<
  GenericSelectorProps<Library>,
  "loadRoute" | "name" | "processor" | "supportAddition"
> & { supportAddition?: boolean };

export const TagSelector = injectIntl((props: TagSelectorProps) => {
  const generator = props.supportAddition
    ? (key: string): TagCreate => ({
        name: key,
        icon: "tag",
      })
    : undefined;

  return (
    <GenericSelector
      loadRoute="/api/tag"
      name={props.intl.formatMessage({ defaultMessage: "Tags" })}
      processor={(v: Tag) => ({ text: v.name, value: v.name })}
      {...props}
      supportAddition={generator}
    />
  );
});

export const LibrarySelector = injectIntl((props: LibrarySelectorProps) => {
  const generator = props.supportAddition
    ? (key: string): LibraryCreate => ({
        name: key,
      })
    : undefined;

  return (
    <GenericSelector
      loadRoute="/api/library"
      name={props.intl.formatMessage({ defaultMessage: "Libraries" })}
      processor={(v: Library) => ({ text: v.name, value: v.name })}
      {...props}
      supportAddition={generator}
    />
  );
});

const saveEntry = (value: any, apiRoute: string) =>
  fetch(apiRoute, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(value),
  })
    .then((r) => r.json())
    .then((j) => {
      if (!j.success) throw new Error(j.error);
      return { ...value, id: j.id };
    });

const GenericSelector = injectIntl<"intl", GenericSelectorProps<any>>(
  <T,>(props: GenericSelectorProps<T>) => {
    const [tags, setTags] = useState<T[] | null>(props.tags ?? null);
    const [isLoading, setLoading] = useState(false);
    const [selected, setSelected] = useState(props.active ?? []);
    const [query, setQuery] = useState("");
    const dropdownRef = useRef(null);

    const { processor } = props;

    useEffect(() => {
      if (!isLoading && tags === null) {
        setLoading(true);
        fetch(props.loadRoute)
          .then((d) => d.json())
          .then((d) => setTags(d.data))
          .then(() => setLoading(false));
      }
    }, [isLoading, tags, props.loadRoute]);

    const options = useMemo(
      () => (tags ?? []).map((v) => processor(v)),
      [tags, processor]
    );

    if (isLoading) {
      return <Loader />;
    }

    let filteredOptions = options.filter(
      (e) => props.exclude === undefined || !props.exclude.includes(e.value)
    );
    if (
      props.supportAddition &&
      query.trim().length > 0 &&
      filteredOptions.findIndex((e) => e.text === query) === -1
    ) {
      filteredOptions.push({
        text: `${props.intl.formatMessage({
          defaultMessage: "Create new tag",
        })}: ${query}`,
        value: `.${query}`,
      });
    }

    return (
      <Dropdown
        placeholder={props.name}
        fluid
        multiple
        search
        selection
        options={filteredOptions}
        ref={dropdownRef}
        value={selected}
        onSearchChange={(_, e) => {
          setQuery(e.searchQuery);
        }}
        onChange={(_, i) => {
          const results = (i.value as string[]).filter(
            (e) => selected.indexOf(e) === -1
          );
          if (results.length === 1) {
            if (
              results[0].startsWith(".") &&
              results[0].substring(1) === query &&
              props.supportAddition
            ) {
              saveEntry(
                props.supportAddition(results[0].substring(1)),
                props.loadRoute
              )
                .then((e) => {
                  setTags((o) => (o ?? []).concat([e]));
                  if (props.onAdded) props.onAdded(e.name);
                })
                .catch((e) => {
                  alert(
                    props.intl.formatMessage({
                      defaultMessage: "Failed to add new tag",
                    })
                  );
                  console.error(e);
                });
              if (props.active === undefined) setSelected(i.value as string[]);
              return;
            } else {
              if (props.onAdded) props.onAdded(results[0]);
            }
          }

          const removed = (selected as string[]).filter(
            (e) => (i.value as string[]).indexOf(e) === -1
          );
          if (removed.length === 1) {
            if (props.onRemoved) props.onRemoved(removed[0]);
          }

          if (props.active === undefined) setSelected(i.value as string[]);
        }}
      />
    );
  }
);

export default TagSelector;
