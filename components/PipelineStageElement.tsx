import React from "react";
import { PipelineStage } from "../@types/Pipeline";
import { Icon, Step } from "semantic-ui-react";
import { FormattedMessage } from "react-intl";

const PipelineStageElement: React.FunctionComponent<
  PipelineStage & { state: "active" | "complete" | "pending" }
> = (props) => {
  const stepProps = {
    ...(props.state === "active" ? { active: true } : {}),
    ...(props.state === "complete" ? { completed: true } : {}),
  };
  switch (props.type) {
    case "form":
      return (
        <Step {...stepProps}>
          <Icon name="file" />
          <Step.Content>
            <Step.Title style={{ color: "#84817a" }}>
              <FormattedMessage defaultMessage={"Data Collection"} />
            </Step.Title>
            <Step.Description>
              <FormattedMessage
                defaultMessage={"Fill in info for each previous record"}
              />
            </Step.Description>
          </Step.Content>
        </Step>
      );
    case "code":
      return (
        <Step {...stepProps}>
          <Icon name="code" />
          <Step.Content>
            <Step.Title style={{ color: "#84817a" }}>
              <FormattedMessage defaultMessage={"Processor"} />
            </Step.Title>
            <Step.Description>
              <FormattedMessage
                defaultMessage={
                  "Run custom code on the results of the previous stage"
                }
              />
            </Step.Description>
          </Step.Content>
        </Step>
      );
    case "upload":
      return (
        <Step {...stepProps}>
          <Icon name="upload" />
          <Step.Content>
            <Step.Title style={{ color: "#84817a" }}>
              <FormattedMessage defaultMessage={"Upload"} />
            </Step.Title>
            <Step.Description>
              <FormattedMessage
                defaultMessage={
                  "Upload image files in bulk and generate a record for each"
                }
              />
            </Step.Description>
          </Step.Content>
        </Step>
      );
    case "create.library":
      return (
        <Step {...stepProps}>
          <Icon name="cubes" />
          <Step.Content>
            <Step.Title style={{ color: "#b33939" }}>
              <FormattedMessage defaultMessage={"Create Library"} />
            </Step.Title>
            <Step.Description>
              <FormattedMessage
                defaultMessage={"Create a library for each record"}
              />
            </Step.Description>
          </Step.Content>
        </Step>
      );
    case "create.tag":
      return (
        <Step {...stepProps}>
          <Icon name="tags" />
          <Step.Content>
            <Step.Title style={{ color: "#b33939" }}>
              <FormattedMessage defaultMessage={"Create Tag"} />
            </Step.Title>
            <Step.Description>
              <FormattedMessage
                defaultMessage={"Create a tag for each record"}
              />
            </Step.Description>
          </Step.Content>
        </Step>
      );
    case "create.asset":
      return (
        <Step {...stepProps}>
          <Icon name="leaf" />
          <Step.Content>
            <Step.Title style={{ color: "#b33939" }}>
              <FormattedMessage defaultMessage={"Create Asset"} />
            </Step.Title>
            <Step.Description>
              <FormattedMessage
                defaultMessage={"Create an asset entry for each record"}
              />
            </Step.Description>
          </Step.Content>
        </Step>
      );
    case "spawn":
      return (
        <Step {...stepProps}>
          <Icon name="grid layout" />
          <Step.Content>
            <Step.Title style={{ color: "#218c74" }}>
              <FormattedMessage defaultMessage={"Create Blank Records"} />
            </Step.Title>
            <Step.Description>
              <FormattedMessage
                defaultMessage={"Create a given number of empty records"}
              />
            </Step.Description>
          </Step.Content>
        </Step>
      );
    case "assign.tags":
      return (
        <Step {...stepProps}>
          <Icon name="tags" />
          <Step.Content>
            <Step.Title style={{ color: "#84817a" }}>
              <FormattedMessage defaultMessage={"Assign Tags"} />
            </Step.Title>
            <Step.Description>
              <FormattedMessage
                defaultMessage={"Assign tags to the current dataset"}
              />
            </Step.Description>
          </Step.Content>
        </Step>
      );
    case "assign.libraries":
      return (
        <Step {...stepProps}>
          <Icon name="boxes" />
          <Step.Content>
            <Step.Title style={{ color: "#84817a" }}>
              <FormattedMessage defaultMessage={"Assign Libraries"} />
            </Step.Title>
            <Step.Description>
              <FormattedMessage
                defaultMessage={"Assign tags to the current dataset"}
              />
            </Step.Description>
          </Step.Content>
        </Step>
      );
  }
};
export default PipelineStageElement;
