import { List } from "semantic-ui-react";
import React, { SyntheticEvent, useCallback, useRef, useState } from "react";
import { DndProvider, useDrag, useDrop, XYCoord } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import {
  SortableContainer,
  SortableContainerProps,
  SortableElement,
} from "react-sortable-hoc";

type ItemType = {
  content: string | React.ReactNode;
  header?: string | React.ReactNode;
  icon?: string;
  image?: string;
  onClick?: (e: SyntheticEvent<HTMLAnchorElement, MouseEvent>) => void;
};

type Props = {
  items: ItemType[];
  onEnd?: (items: ItemType[]) => void;
  active?: number;
  rerenderOn?: any;
};

type SortableItem = Props["items"][number] & {
  index: number;
  active: boolean;
};

const Item: React.FunctionComponent<SortableItem> = (props) => {
  return (
    <List.Item
      // data-index={props.index}
      // content={props.content}
      // icon={props.icon}
      // header={props.header}
      // image={props.image}
      {...props}
    ></List.Item>
  );
};

const OuterItem = SortableElement(Item);

const InnerSortableList: React.ComponentClass<Props & SortableContainerProps> =
  SortableContainer((props: Props) => {
    return (
      <List selection animated>
        {props.items.map((e, i) => (
          <OuterItem
            {...e}
            /*@ts-ignore*/
            active={i === props.active}
            index={i}
            key={i}
          />
        ))}
      </List>
    );
  });

const SortableList: React.FunctionComponent<Props> = (props) => {
  const [state, setState] = useState(props.items);

  return (
    // @ts-ignore
    <InnerSortableList
      items={state}
      onSortEnd={(v) => {
        const clone = [...state];
        const temp = clone[v.newIndex];
        clone[v.newIndex] = clone[v.oldIndex];
        clone[v.oldIndex] = temp;
        if (props.onEnd) props.onEnd(clone);
        else setState(clone);
      }}
      distance={2}
      active={props.active}
    />
  );
};

export default SortableList;
