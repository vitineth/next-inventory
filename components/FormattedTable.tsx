import React from "react";
import { Icon, Image, Label, Table } from "semantic-ui-react";
import Link from "next/link";
import RenderedProperty from "./RenderedProperty";
import zod from "zod";

export const ColumnSchemaValidator = zod.array(
  zod.object({
    display: zod.string(),
    access: zod.string(),
    format: zod
      .enum(["text", "image", "link", "code", "icon"])
      .or(
        zod.function().args(zod.any()).returns(zod.custom<React.ReactNode>())
      ),
  })
);

export const TableSchemaValidator = zod.object({
  columns: ColumnSchemaValidator,
});

export type Format =
  | ("text" | "image" | "link" | "code" | "icon")
  | ((d: any) => React.ReactNode);

export type TableSchema = {
  columns: { display: string; access: string; format: Format }[];
};

export type FormattedTableProps = {
  configuration: TableSchema;
  data: Record<string, any>[];
};

const FormattedTable: React.FC<FormattedTableProps> = ({
  configuration: schema,
  data,
}) => {
  return (
    <Table selectable celled>
      <Table.Header>
        <Table.Row>
          {schema.columns.map((column) => (
            <Table.HeaderCell key={column.access}>
              {column.display}
            </Table.HeaderCell>
          ))}
        </Table.Row>
      </Table.Header>

      <Table.Body>
        {data.map((row) => (
          <Table.Row key={row.id}>
            {schema.columns.map((column) => (
              <Table.Cell key={`${row.id}.${column.access}`}>
                <RenderedProperty
                  schema={column}
                  value={(row as any)[column.access]}
                />
              </Table.Cell>
            ))}
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
  );
};

export default FormattedTable;
