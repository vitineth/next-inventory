import React from "react";
import { Container, Dropdown, Menu } from "semantic-ui-react";
import Icon from "@mdi/react";
import { mdiDatabaseSettings } from "@mdi/js";
import { FormattedMessage, injectIntl } from "react-intl";
import { IntlShape } from "react-intl/src/types";

const SharedHeader: React.FunctionComponent<{
  children?: React.ReactNode;
  intl: IntlShape;
}> = (props, context) => (
  <>
    <Menu fixed="top" inverted>
      <Container>
        <Menu.Item as="a" header link href="/">
          <Icon
            size={1.5}
            color={"#94DBC5"}
            style={{ marginRight: "10px" }}
            path={mdiDatabaseSettings}
          />
          <FormattedMessage defaultMessage={"Next Asset Inventory"} />
        </Menu.Item>
        <Menu.Item as="a" link href="/">
          <FormattedMessage defaultMessage={"Home"} />
        </Menu.Item>
        <Menu.Item as="a" link href="/asset">
          <FormattedMessage defaultMessage={"Search"} />
        </Menu.Item>
        <Dropdown
          item
          simple
          text={props.intl.formatMessage({ defaultMessage: "Assets" })}
        >
          <Dropdown.Menu>
            <Dropdown.Item as="a" href="/asset">
              <FormattedMessage defaultMessage={"Manage"} />
            </Dropdown.Item>
            <Dropdown.Item as="a" href="/asset/create">
              <FormattedMessage defaultMessage={"Add"} />
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
        <Dropdown
          item
          simple
          text={props.intl.formatMessage({ defaultMessage: "Tags" })}
        >
          <Dropdown.Menu>
            <Dropdown.Item as="a" href="/tag">
              <FormattedMessage defaultMessage={"Manage"} />
            </Dropdown.Item>
            <Dropdown.Item as="a" href="/tag/create">
              <FormattedMessage defaultMessage={"Add"} />
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
        <Dropdown
          item
          simple
          text={props.intl.formatMessage({ defaultMessage: "Libraries" })}
        >
          <Dropdown.Menu>
            <Dropdown.Item as="a" href="/library">
              <FormattedMessage defaultMessage={"Manage"} />
            </Dropdown.Item>
            <Dropdown.Item as="a" href="/library/create">
              <FormattedMessage defaultMessage={"Add"} />
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
        <Menu.Item as="a" href="/schema">
          <FormattedMessage defaultMessage={"Configure"} />
        </Menu.Item>
        <Dropdown
          item
          simple
          text={props.intl.formatMessage({ defaultMessage: "Pipelines" })}
        >
          <Dropdown.Menu>
            <Dropdown.Item as="a" href="/pipeline">
              <FormattedMessage defaultMessage={"Manage"} />
            </Dropdown.Item>
            <Dropdown.Item as="a" href="/pipeline/create">
              <FormattedMessage defaultMessage={"Add"} />
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </Container>
    </Menu>
    <Container style={{ marginTop: "7em" }}>{props.children}</Container>
  </>
);

export default injectIntl(SharedHeader);
