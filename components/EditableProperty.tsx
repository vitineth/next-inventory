// Edit properties! Like UEMS!
import { TableSchema } from "./FormattedTable";
import {
  Button,
  Form,
  FormInputProps,
  Header,
  HeaderProps,
  Popup,
} from "semantic-ui-react";
import React, { CSSProperties, useMemo, useState } from "react";
import RenderedProperty from "./RenderedProperty";
import IconDropdown from "./IconDropdown";
import { injectIntl, IntlShape } from "react-intl";

type EditablePropertyProps = {
  schema: TableSchema["columns"][number];
  value?: string;
  onChange: (v: string | undefined) => void;

  blankIfUndefined?: boolean;
  includeHeader?: boolean;
  headerSize?: HeaderProps["size"];

  style?: CSSProperties;
  className?: string;

  intl: IntlShape;
};

const EditableProperty = injectIntl((props: EditablePropertyProps) => {
  const [editable, setEditable] = useState(false);
  const [value, setValue] = useState(props.value);

  let header = useMemo(
    () =>
      props.includeHeader ? (
        <Header size={props.headerSize}>{props.schema.display}</Header>
      ) : undefined,
    [props.headerSize, props.schema.display, props.includeHeader]
  );

  if (!editable) {
    return (
      <>
        <RenderedProperty
          schema={props.schema}
          blankIfUndefined={props.blankIfUndefined}
          value={props.value}
          headerSize={props.headerSize}
          includeHeader={props.includeHeader}
        />

        <Popup
          content={props.intl.formatMessage({ defaultMessage: "Edit" })}
          position="top left"
          inverted
          trigger={
            <Button
              circular
              icon="edit"
              size="tiny"
              style={{ marginLeft: "30px", fontSize: "15px" }}
              onClick={() => setEditable(true)}
            />
          }
        />
      </>
    );
  }

  const buttons = (
    <Button.Group style={{ marginTop: "10px" }}>
      <Popup
        content={props.intl.formatMessage({ defaultMessage: "Save" })}
        position="top left"
        inverted
        trigger={
          <Button
            icon="save"
            onClick={() => {
              props.onChange(value);
              setEditable(false);
            }}
          />
        }
      />
      <Popup
        content={props.intl.formatMessage({ defaultMessage: "Cancel" })}
        position="top left"
        inverted
        trigger={
          <Button
            icon="delete"
            onClick={() => {
              setValue(props.value);
              setEditable(false);
            }}
          />
        }
      />
      <Popup
        content={props.intl.formatMessage({ defaultMessage: "Unset" })}
        position="top left"
        inverted
        trigger={
          <Button
            icon="trash"
            onClick={() => {
              setValue(undefined);
              setEditable(false);
              props.onChange(undefined);
            }}
          />
        }
      />
    </Button.Group>
  );

  const iconLookup: Record<string, FormInputProps> = {
    code: { icon: "code", iconPosition: "left" },
    link: { icon: "chain", iconPosition: "left" },
  };

  switch (props.schema.format) {
    case "icon":
      return (
        <>
          {header}
          <IconDropdown
            value={props.value}
            onIconSelected={(v) => setValue(v)}
          />
          {buttons}
        </>
      );
    case "code":
    case "link":
    case "text":
      return (
        <>
          {header}
          <Form.Input
            fluid
            {...(iconLookup[props.schema.format] ?? {})}
            value={value}
            onChange={(_, d) => setValue(d.value)}
          />
          {buttons}
        </>
      );
    case "image":
      return (
        <>
          {header}
          <Form.Input
            fluid
            type="file"
            accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*"
            onChange={(c) => {
              const files = c.target.files;
              if (files && files.length > 0) {
                const reader = new FileReader();
                reader.readAsDataURL(files[0]);
                reader.onloadend = () => {
                  if (typeof reader.result === "string")
                    setValue(reader.result);
                  else if (
                    typeof reader.result === "object" &&
                    reader.result !== null
                  )
                    setValue(
                      btoa(
                        String.fromCharCode.apply(
                          null,
                          new Uint8Array(reader.result) as any
                        )
                      )
                    );
                };
              }
            }}
          />
          {buttons}
        </>
      );
  }

  return <>TODO</>;
});
export default EditableProperty;
