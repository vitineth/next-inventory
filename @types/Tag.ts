export type TagCreate = {
  name: string;
  icon: string;
} & Record<string, any>;

export type ShallowTag = {
  id: string;
  name: string;
  icon: string;
} & Record<string, any>;

export type Tag = {
  id: string;
  name: string;
  icon: string;
} & Record<string, any>;
