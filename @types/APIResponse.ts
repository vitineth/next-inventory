export type ModificationResponse =
  | {
      success: true;
      id?: string;
    }
  | {
      success: false;
      error: string;
    };

export type QueryResponse<T> =
  | {
      success: true;
      data: T;
    }
  | {
      success: false;
      error: string;
    };
