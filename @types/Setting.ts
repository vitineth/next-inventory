import { ObjectId } from "mongodb";
import { TableSchema } from "../components/FormattedTable";

export type SettingSchema = { _id: ObjectId; key: string; value: TableSchema };
