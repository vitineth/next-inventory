import { ShallowAsset } from "./Asset";

export type LibraryCreate = {
  name: string;
} & Record<string, any>;

export type ShallowLibrary = {
  id: string;
  name: string;
} & Record<string, any>;

export type Library = {
  id: string;
  name: string;
} & Record<string, any>;
