import { ShallowLibrary } from "./Library";
import { ShallowTag } from "./Tag";
import zod from "zod";

export const AssetUpdateValidator = zod
  .object({
    ".unset": zod.array(zod.string()).optional(),
    ".library": zod
      .object({
        ".add": zod.array(zod.string()).optional(),
        ".remove": zod.array(zod.string()).optional(),
      })
      .optional(),
    ".tag": zod
      .object({
        ".add": zod.array(zod.string()).optional(),
        ".remove": zod.array(zod.string()).optional(),
      })
      .optional(),
  })
  .passthrough()
  .refine(
    (d) =>
      !("identifier" in d) &&
      !("id" in d) &&
      !("tags" in d) &&
      !("libraries" in d),
    "Forbidden keys present"
  );

export type AssetUpdate = zod.infer<typeof AssetUpdateValidator>;

export type CreateAsset = {
  identifier: string;
  tags?: string[];
  libraries?: string[];
} & Record<string, any>;

export type ShallowAsset = {
  id: string;
  identifier: string;
  tags: string[];
  libraries: string[];
} & Record<string, any>;

export type Asset = {
  id: string;
  identifier: string;
  tags: ShallowTag[];
  libraries: ShallowLibrary[];
} & Record<string, any>;
