import zod from "zod";
import { ColumnSchemaValidator } from "../components/FormattedTable";

export const NRecordValidator = zod.object({
  type: zod.literal("spawn"),
  amount: zod.number(),
});
export type NRecord = zod.infer<typeof NRecordValidator>;

export const BulkImageUploadValidator = zod.object({
  type: zod.literal("upload"),
  sizeMax: zod.number(),
  key: zod.string(),
});
export type BulkImageUpload = zod.infer<typeof BulkImageUploadValidator>;

export const FormForEachValidator = zod.object({
  type: zod.literal("form"),
  form: ColumnSchemaValidator,
});
export type FormForEach = zod.infer<typeof FormForEachValidator>;

export const CustomCodeValidator = zod.object({
  type: zod.literal("code"),
  code: zod.string(),
});
export type CustomCode = zod.infer<typeof CustomCodeValidator>;

export const CreateTagForEachValidator = zod.object({
  type: zod.literal("create.tag"),
});
export type CreateTagForEach = zod.infer<typeof CreateTagForEachValidator>;

export const CreateAssetForEachValidator = zod.object({
  type: zod.literal("create.asset"),
});
export type CreateAssetForEach = zod.infer<typeof CreateAssetForEachValidator>;

export const CreateLibraryForEachValidator = zod.object({
  type: zod.literal("create.library"),
});
export type CreateLibraryForEach = zod.infer<
  typeof CreateLibraryForEachValidator
>;

export const AssignTagsValidator = zod.object({
  type: zod.literal("assign.tags"),
});
export type AssignTags = zod.infer<typeof AssignTagsValidator>;

export const AssignLibrariesValidator = zod.object({
  type: zod.literal("assign.libraries"),
});
export type AssignLibraries = zod.infer<typeof AssignLibrariesValidator>;

export const PipelineStageValidator = BulkImageUploadValidator.or(
  FormForEachValidator
)
  .or(CustomCodeValidator)
  .or(CreateTagForEachValidator)
  .or(CreateAssetForEachValidator)
  .or(CreateLibraryForEachValidator)
  .or(NRecordValidator)
  .or(AssignLibrariesValidator)
  .or(AssignTagsValidator);
export type PipelineStage = zod.infer<typeof PipelineStageValidator>;

export const PipelineValidator = zod.object({
  stages: zod.array(PipelineStageValidator),
  name: zod.string(),
});
export type Pipeline = zod.infer<typeof PipelineValidator>;

export const PipelineUpdateValidator = PipelineValidator.omit({
  name: true,
}).extend({
  ".unset": zod.array(zod.string()).optional(),
});
export type PipelineUpdate = zod.infer<typeof PipelineUpdateValidator>;
