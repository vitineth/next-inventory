import { MongoClient, ObjectId } from "mongodb";
import { SettingSchema } from "../@types/Setting";

const uri = process.env.MONGODB_URI;
const options = {};

let client: MongoClient;
let clientPromise: Promise<MongoClient>;

if (!uri) {
  throw new Error("Please add your Mongo URI to .env.local");
}

export async function validateSettings(client: MongoClient) {
  const collection = client
    .db("next-inventory")
    .collection<SettingSchema>("settings");
  await collection.createIndex({ key: 1 }, { unique: true });
  await collection.updateOne(
    {
      key: "schema.tag",
    },
    {
      $setOnInsert: { value: { columns: [] } },
    },
    {
      upsert: true,
    }
  );

  await collection.updateOne(
    {
      key: "schema.library",
    },
    {
      $setOnInsert: { value: { columns: [] } },
    },
    {
      upsert: true,
    }
  );

  await collection.updateOne(
    {
      key: "schema.asset",
    },
    {
      $setOnInsert: { value: { columns: [] } },
    },
    {
      upsert: true,
    }
  );
}

export async function validateTags(client: MongoClient) {
  // const client = await pending;
  return await client.db("next-inventory").collection("tag").createIndex(
    {
      name: 1,
    },
    {
      unique: true,
    }
  );
}

export async function validateTextIndex(client: MongoClient) {
  return await client.db("next-inventory").collection("asset").createIndex({
    "$**": "text",
  });
}

export async function validate(client: MongoClient) {
  return Promise.allSettled([
    validateTags(client),
    validateSettings(client),
    validateTextIndex(client),
  ]);
}

if (process.env.NODE_ENV === "development") {
  // In development mode, use a global variable so that the value
  // is preserved across module reloads caused by HMR (Hot Module Replacement).
  if (!(global as any)._mongoClientPromise) {
    client = new MongoClient(uri, options);
    (global as any)._mongoClientPromise = client
      .connect()
      .then(async (c: MongoClient) => {
        await validate(c);
        return c;
      });
  }
  clientPromise = (global as any)._mongoClientPromise;
} else {
  // In production mode, it's best to not use a global variable.
  client = new MongoClient(uri, options);
  clientPromise = client.connect().then(async (c) => {
    await validate(c);
    return c;
  });
}

// Export a module-scoped MongoClient promise. By doing this in a
// separate module, the client can be shared across functions.
export default clientPromise;
