import pending from "../mongodb";
import { Tag, TagCreate } from "../../@types/Tag";
import { Asset } from "../../@types/Asset";
import { Pipeline } from "../../@types/Pipeline";

export async function getPipeline(id: string): Promise<Pipeline | null> {
  const client = await pending;
  const result = await client
    .db("next-inventory")
    .collection<Pipeline>("pipeline")
    .findOne({
      name: id,
    });
  if (result === null) return null;

  const { _id, ...rest } = result;
  return {
    id: _id.toString(),
    ...rest,
  } as Pipeline;
}

export async function getAllPipelines(): Promise<Pipeline[]> {
  const client = await pending;
  return (
    await client
      .db("next-inventory")
      .collection<Pipeline>("pipeline")
      .find()
      .toArray()
  ).map((value) => {
    const { _id, ...rest } = value;

    return { id: _id.toString(), ...rest } as Pipeline;
  });
}

export async function updatePipeline(
  name: string,
  changes: Omit<Pipeline, "name"> & { ".unset"?: string[] }
) {
  const client = await pending;

  const { ".unset": unset, ...rest } = changes;
  let mappedUnset =
    unset === undefined
      ? undefined
      : Object.fromEntries((unset as string[]).map((v) => [v, ""]));

  if (Object.keys(rest).length === 0 && mappedUnset === undefined)
    return {
      matchedCount: 1,
    };

  return client
    .db("next-inventory")
    .collection("pipeline")
    .updateOne(
      {
        name,
      },
      {
        ...(Object.keys(rest).length === 0 ? undefined : { $set: rest }),
      }
    );
}

export async function deletePipeline(name: string) {
  const client = await pending;
  return client.db("next-inventory").collection("pipeline").deleteOne({
    name,
  });
}

export async function getPipelineCount() {
  const client = await pending;
  return await client
    .db("next-inventory")
    .collection("pipeline")
    .countDocuments();
}

export async function createPipeline(pipeline: Pipeline) {
  const client = await pending;
  return await client
    .db("next-inventory")
    .collection("pipeline")
    .insertOne(pipeline);
}
