import pending from "../mongodb";
import { SettingSchema } from "../../@types/Setting";
import { TableSchema } from "../../components/FormattedTable";

export const defaultTagSchema: TableSchema["columns"] = [
  // {display: 'Icon', access: 'icon', format: 'icon'},
  // {display: 'Name', access: 'name', format: 'text'},
];

export const defaultAssetSchema: TableSchema["columns"] = [
  // {display: 'Name', access: 'name', format: 'text'},
];

export const defaultLibrarySchema: TableSchema["columns"] = [
  // {display: 'Name', access: 'name', format: 'text'},
];

export async function deleteKeyFromSchema(
  key: string,
  entity: "tag" | "asset" | "library"
): Promise<TableSchema | null> {
  const client = await pending;
  const result = await client
    .db("next-inventory")
    .collection<SettingSchema>("settings")
    .findOneAndUpdate(
      {
        key: `schema.${entity}`,
        "value.columns.access": key,
      },
      {
        $pull: {
          "value.columns": { access: key },
        },
      },
      {
        returnDocument: "after",
      }
    );

  if (result === null || result.value === null) return null;
  return result.value.value;
}

export async function purgeKeyAndDataFromSchema(
  key: string,
  entity: "tag" | "asset" | "library"
): Promise<TableSchema | null> {
  const client = await pending;
  const result = await client
    .db("next-inventory")
    .collection<SettingSchema>("settings")
    .findOneAndUpdate(
      {
        key: `schema.${entity}`,
        "value.columns.access": key,
      },
      {
        $pull: {
          "value.columns": { access: key },
        },
      },
      {
        returnDocument: "after",
      }
    );
  if (result === null || result.value === null) return null;

  // Now that the element is gone, we need to remove the data from the entities
  await client
    .db("next-inventory")
    .collection<SettingSchema>(entity)
    .updateMany(
      {
        [key]: { $exists: true },
      },
      {
        $unset: { [key]: "" },
      }
    );

  // Then return the updated document
  return result.value.value;
}

export async function updateSchemaEntry(
  key: string,
  entity: "tag" | "asset" | "library",
  replacement: TableSchema["columns"][number]
): Promise<TableSchema | null> {
  const client = await pending;
  const result = await client
    .db("next-inventory")
    .collection<SettingSchema>("settings")
    .findOneAndUpdate(
      {
        key: `schema.${entity}`,
        "value.columns.access": key,
      },
      {
        $set: {
          "value.columns.$": replacement,
        },
      },
      {
        returnDocument: "after",
      }
    );

  if (result === null || result.value === null) return null;
  return result.value.value;
}

export async function addSchemaEntry(
  entity: "tag" | "asset" | "library",
  property: TableSchema["columns"][number]
): Promise<TableSchema | null> {
  const client = await pending;
  const result = await client
    .db("next-inventory")
    .collection<SettingSchema>("settings")
    .findOneAndUpdate(
      {
        key: `schema.${entity}`,
      },
      {
        $push: {
          "value.columns": property,
        },
      },
      {
        returnDocument: "after",
      }
    );

  if (result === null || result.value === null) return null;
  return result.value.value;
}

export async function getTagsSchema(): Promise<TableSchema> {
  const client = await pending;
  const result = (
    await client
      .db("next-inventory")
      .collection<SettingSchema>("settings")
      .findOne({
        key: "schema.tag",
      })
  )?.value;

  if (result === undefined) return { columns: defaultTagSchema };
  return { columns: [...result.columns, ...defaultTagSchema] };
}

export async function getAssetSchema(): Promise<TableSchema> {
  const client = await pending;
  const result = (
    await client
      .db("next-inventory")
      .collection<SettingSchema>("settings")
      .findOne({
        key: "schema.asset",
      })
  )?.value;

  if (result === undefined) return { columns: defaultAssetSchema };
  return { columns: [...result.columns, ...defaultAssetSchema] };
}

export async function getLibrarySchema(): Promise<TableSchema> {
  const client = await pending;
  const result = (
    await client
      .db("next-inventory")
      .collection<SettingSchema>("settings")
      .findOne({
        key: "schema.library",
      })
  )?.value;

  if (result === undefined) return { columns: defaultLibrarySchema };
  return { columns: [...result.columns, ...defaultLibrarySchema] };
}
