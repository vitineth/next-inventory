import pending from "../mongodb";
import { Tag, TagCreate } from "../../@types/Tag";
import { Asset } from "../../@types/Asset";

export async function getTag(id: string): Promise<Tag | null> {
  const client = await pending;
  const result = await client.db("next-inventory").collection("tag").findOne({
    name: id,
  });
  if (result === null) return null;

  const { _id, ...rest } = result;
  return {
    id: _id.toString(),
    ...rest,
  } as Tag;
}

export async function getAllTags(): Promise<Tag[]> {
  const client = await pending;
  return (
    await client.db("next-inventory").collection("tag").find().toArray()
  ).map((value) => {
    const { _id, ...rest } = value;

    return { id: _id.toString(), ...rest } as Tag;
  });
}

export async function updateTag(
  name: string,
  changes: Omit<TagCreate, "name">
) {
  const client = await pending;

  const { ".unset": unset, ...rest } = changes;
  let mappedUnset =
    unset === undefined
      ? undefined
      : Object.fromEntries((unset as string[]).map((v) => [v, ""]));

  if (Object.keys(rest).length === 0 && mappedUnset === undefined)
    return {
      modifiedCount: 1,
    };

  return client
    .db("next-inventory")
    .collection("tag")
    .updateOne(
      {
        name,
      },
      {
        ...(Object.keys(rest).length === 0 ? undefined : { $set: rest }),
        ...(mappedUnset === undefined ? undefined : { $unset: mappedUnset }),
      }
    );
}

export async function deleteTag(name: string) {
  const client = await pending;
  return client.db("next-inventory").collection("tag").deleteOne({
    name,
  });
}

export async function getTagCount() {
  const client = await pending;
  return await client.db("next-inventory").collection("tag").countDocuments();
}

export async function createTag(tag: TagCreate) {
  const client = await pending;
  return await client.db("next-inventory").collection("tag").insertOne(tag);
}
