import pending from "../mongodb";
import {
  Asset,
  AssetUpdate,
  CreateAsset,
  ShallowAsset,
} from "../../@types/Asset";
import { Filter, ObjectId, UpdateFilter } from "mongodb";
import { ShallowTag } from "../../@types/Tag";
import { ShallowLibrary } from "../../@types/Library";
import sharp from "sharp";
import { inspect } from "util";

export type AssetQuery = {
  tags?: string[];
  libraries?: string[];
  query?: string;
};

const renameTagSelfAndLibraryID = (value: any) => {
  const { _id, ...rest } = value;

  value.tags.map((e: any) => {
    e.id = e._id.toString();
    delete e._id;
  });

  value.libraries.map((e: any) => {
    e.id = e._id.toString();
    delete e._id;
  });

  return { id: _id.toString(), ...rest } as Asset;
};

const paginationPipeline = (id: string, pageSize: number = 40) => [
  {
    $match: {
      _id: { $gt: new ObjectId(id) },
    },
  },
  {
    $limit: pageSize,
  },
];

const resolveTagAndLibraryPipeline = [
  {
    $addFields: {
      tags: {
        $map: {
          input: "$tags",
          in: {
            $toObjectId: "$$this",
          },
        },
      },
    },
  },
  {
    $addFields: {
      libraries: {
        $map: {
          input: "$libraries",
          in: {
            $toObjectId: "$$this",
          },
        },
      },
    },
  },
  {
    $lookup: {
      from: "tag",
      localField: "tags",
      foreignField: "_id",
      as: "tags",
    },
  },
  {
    $lookup: {
      from: "library",
      localField: "libraries",
      foreignField: "_id",
      as: "libraries",
    },
  },
];

export async function getAsset(id: string): Promise<Asset | null> {
  const client = await pending;
  const records = await client
    .db("next-inventory")
    .collection("asset")
    .aggregate([
      { $match: { identifier: id } },
      {
        $addFields: {
          tags: {
            $map: {
              input: "$tags",
              in: {
                $toObjectId: "$$this",
              },
            },
          },
        },
      },
      {
        $addFields: {
          libraries: {
            $map: {
              input: "$libraries",
              in: {
                $toObjectId: "$$this",
              },
            },
          },
        },
      },
      {
        $lookup: {
          from: "tag",
          localField: "tags",
          foreignField: "_id",
          as: "tags",
        },
      },
      {
        $lookup: {
          from: "library",
          localField: "libraries",
          foreignField: "_id",
          as: "libraries",
        },
      },
    ])
    .toArray();
  if (records.length === 0) return null;

  records[0].tags.map((e: any) => {
    e.id = e._id.toString();
    delete e._id;
  });

  records[0].libraries.map((e: any) => {
    e.id = e._id.toString();
    delete e._id;
  });

  const { _id, ...rest } = records[0];

  return {
    id: _id.toString(),
    ...rest,
  } as Asset;
}

export async function createAsset(asset: CreateAsset) {
  const client = await pending;
  let tags: string[] = [];
  let libraries: string[] = [];

  let waiting: Promise<any>[] = [];
  if (asset.tags && asset.tags.length > 0) {
    waiting.push(
      client
        .db("next-inventory")
        .collection("tag")
        .find({
          name: { $in: asset.tags },
        })
        .toArray()
        .then((e) => e.map((v) => v._id.toString()))
        .then((d) => tags.push(...d))
    );
  }
  if (asset.libraries && asset.libraries.length > 0) {
    waiting.push(
      client
        .db("next-inventory")
        .collection("library")
        .find({
          name: { $in: asset.libraries },
        })
        .toArray()
        .then((e) => e.map((v) => v._id.toString()))
        .then((d) => libraries.push(...d))
    );
  }

  await Promise.all(waiting);

  return await client
    .db("next-inventory")
    .collection("asset")
    .insertOne({
      ...asset,
      tags,
      libraries,
    });
}

export async function updateAsset(
  identifier: string,
  update: AssetUpdate
): Promise<{
  modified: number;
  addedTags: ShallowTag[];
  removedTags: ShallowTag[];
  removedLibraries: ShallowLibrary[];
  addedLibraries: ShallowLibrary[];
}> {
  const client = await pending;
  const modification: UpdateFilter<{
    _id: ObjectId;
    identifier: string;
    tags: string[];
    libraries: string[];
  }> = {};

  let removedTags: ShallowTag[] = [];
  let addedTags: ShallowTag[] = [];
  let removedLibraries: ShallowLibrary[] = [];
  let addedLibraries: ShallowLibrary[] = [];

  if (update[".tag"]) {
    if (update[".tag"][".add"]) {
      // Need to lookup the tags
      const entries = await client
        .db("next-inventory")
        .collection("tag")
        .find({
          name: {
            $in: update[".tag"][".add"],
          },
        })
        .toArray();

      addedTags = entries.map((e) => {
        const { _id, ...rest } = e;
        return { id: _id.toString(), ...rest };
      }) as any;

      modification.$addToSet = {
        // @ts-ignore - so according to the docs this is valid but the types don't like it?
        tags: { $each: entries.map((e) => e._id.toString()) },
      };
    }
    if (update[".tag"][".remove"]) {
      // Need to lookup the tags
      const entries = await client
        .db("next-inventory")
        .collection("tag")
        .find({
          name: {
            $in: update[".tag"][".remove"],
          },
        })
        .toArray();

      removedTags = entries.map((e) => {
        const { _id, ...rest } = e;
        return { id: _id.toString(), ...rest };
      }) as any;

      modification.$pullAll = {
        tags: entries.map((e) => e._id.toString()),
      };
    }
  }

  if (update[".library"]) {
    if (update[".library"][".add"]) {
      const entries = await client
        .db("next-inventory")
        .collection("library")
        .find({
          name: {
            $in: update[".library"][".add"],
          },
        })
        .toArray();

      addedLibraries = entries.map((e) => {
        const { _id, ...rest } = e;
        return { id: _id.toString(), ...rest };
      }) as any;

      modification.$addToSet = {
        ...(modification.$addToSet ?? {}),
        // @ts-ignore - so according to the docs this is valid but the types don't like it?
        libraries: { $each: entries.map((e) => e._id.toString()) },
      };
    }
    if (update[".library"][".remove"]) {
      const entries = await client
        .db("next-inventory")
        .collection("library")
        .find({
          name: {
            $in: update[".library"][".remove"],
          },
        })
        .toArray();

      removedTags = entries.map((e) => {
        const { _id, ...rest } = e;
        return { id: _id.toString(), ...rest };
      }) as any;

      // @ts-ignore
      modification.$pullAll = {
        ...(modification.$pullAll ?? {}),
        // @ts-ignore - so according to the docs this is valid but the types don't like it?
        libraries: entries.map((e) => e._id.toString()),
      };
    }
  }

  if (update[".unset"]) {
    modification.$unset = Object.fromEntries(
      update[".unset"].map((e) => [e, ""])
    );
  }

  const { ".unset": _u, ".library": _l, ".tag": _t, ...rest } = update;

  if (Object.keys(rest).length > 0) modification.$set = rest;

  let updateResult = await client
    .db("next-inventory")
    .collection("asset")
    .updateOne(
      {
        identifier,
      },
      modification
    );

  return {
    removedTags,
    addedTags,
    addedLibraries,
    removedLibraries,
    modified: updateResult.modifiedCount,
  };
}

export async function deleteAsset(identifier: string) {
  const client = await pending;
  return client.db("next-inventory").collection("asset").deleteOne({
    identifier,
  });
}

export async function getAssetCount() {
  const client = await pending;
  return await client.db("next-inventory").collection("asset").countDocuments();
}

export async function getAllAssets(
  paginationID?: string | null
): Promise<Asset[]> {
  const client = await pending;

  const aggregation = [];
  if (paginationID === null) aggregation.push({ $limit: 40 });
  else if (paginationID) aggregation.push(...paginationPipeline(paginationID));
  aggregation.push(...resolveTagAndLibraryPipeline);

  return (
    await client
      .db("next-inventory")
      .collection("asset")
      .aggregate(aggregation)
      .toArray()
  ).map(renameTagSelfAndLibraryID);
}

export async function queryAsset(
  page: string | null | undefined,
  query: AssetQuery
) {
  const client = await pending;

  let tagPromise: Promise<string[]>;
  let libraryPromise: Promise<string[]>;

  if (query.tags && query.tags.length > 0) {
    tagPromise = client
      .db("next-inventory")
      .collection("tag")
      .find({
        name: { $in: query.tags },
      })
      .toArray()
      .then((array) => array.map((v) => v._id.toString()));
  } else {
    tagPromise = Promise.resolve([]);
  }

  if (query.libraries && query.libraries.length > 0) {
    libraryPromise = client
      .db("next-inventory")
      .collection("library")
      .find({
        name: { $in: query.libraries },
      })
      .toArray()
      .then((array) => array.map((v) => v._id.toString()));
  } else {
    libraryPromise = Promise.resolve([]);
  }

  const [tagSet, librarySet] = await Promise.all([tagPromise, libraryPromise]);
  const aggregationPipeline = [];

  if (query.query) {
    aggregationPipeline.push({
      $match: {
        $text: {
          $search: query.query,
        },
      },
    });
  }

  if (tagSet.length > 0 || librarySet.length > 0) {
    aggregationPipeline.push({
      $match: {
        ...(tagSet.length > 0 ? { tags: { $in: tagSet } } : {}),
        ...(librarySet.length > 0 ? { libraries: { $in: librarySet } } : {}),
      },
    });
  }

  if (page === null) aggregationPipeline.push({ $limit: 40 });
  else if (page) aggregationPipeline.push(...paginationPipeline(page));

  console.log(
    "searching with filters",
    inspect(aggregationPipeline, false, null, true)
  );

  return await client
    .db("next-inventory")
    .collection("asset")
    .aggregate([...aggregationPipeline, ...resolveTagAndLibraryPipeline])
    .toArray()
    .then((result) => result.map(renameTagSelfAndLibraryID));
}

export async function compressAsset(asset: Asset): Promise<[number, Asset]> {
  let total = 0;
  let result = await Promise.all(
    Object.entries(asset).map(async ([key, value]) => {
      if (typeof value === "string" && value.startsWith("data:image/")) {
        total++;
        return sharp(new Buffer(value.substring(22), "base64"))
          .resize(100)
          .toFormat("png")
          .toBuffer()
          .then((b) => {
            return [key, `data:image/png;base64,${b.toString("base64")}`];
          });
      }

      return [key, value];
    })
  ).then((vs) => Object.fromEntries(vs));
  return [total, result];
}

export function compressAssets(assets: Asset[]): Promise<Asset[]> {
  console.log("Trying to compress");
  return Promise.all(assets.map(compressAsset)).then((values) => {
    const total = values.map((e) => e[0]).reduce((a, b) => a + b, 0);
    console.log(`Compressed ${total}/${assets.length}`);
    return values.map((e) => e[1]);
  });
}
