import pending from "../mongodb";
import { Library, LibraryCreate } from "../../@types/Library";
import { Tag } from "../../@types/Tag";

export async function getLibrary(id: string): Promise<Library | null> {
  const client = await pending;
  const result = await client
    .db("next-inventory")
    .collection("library")
    .findOne({
      name: id,
    });
  if (result === null) return null;

  const { _id, ...rest } = result;
  return {
    id: _id.toString(),
    ...rest,
  } as Library;
}

export async function getAllLibraries(): Promise<Library[]> {
  const client = await pending;
  return (
    await client.db("next-inventory").collection("library").find().toArray()
  ).map((value) => {
    const { _id, ...rest } = value;

    return { id: _id.toString(), ...rest } as Library;
  });
}

export async function updateLibrary(
  name: string,
  changes: Omit<LibraryCreate, "name">
) {
  const client = await pending;

  const { ".unset": unset, ...rest } = changes;
  let mappedUnset =
    unset === undefined
      ? undefined
      : Object.fromEntries((unset as string[]).map((v) => [v, ""]));

  if (Object.keys(rest).length === 0 && mappedUnset === undefined)
    return {
      modifiedCount: 1,
    };

  return client
    .db("next-inventory")
    .collection("library")
    .updateOne(
      {
        name,
      },
      {
        ...(Object.keys(rest).length === 0 ? undefined : { $set: rest }),
        ...(mappedUnset === undefined ? undefined : { $unset: mappedUnset }),
      }
    );
}

export async function deleteLibrary(name: string) {
  const client = await pending;
  return client.db("next-inventory").collection("library").deleteOne({
    name,
  });
}

export async function createLibrary(library: LibraryCreate) {
  const client = await pending;
  return await client
    .db("next-inventory")
    .collection("library")
    .insertOne(library);
}

export async function getLibraryCount() {
  const client = await pending;
  return await client
    .db("next-inventory")
    .collection("library")
    .countDocuments();
}
